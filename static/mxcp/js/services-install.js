// Equivalent to jquery's "$(document).ready()"
document.addEventListener("DOMContentLoaded", function()
{
    /* Get value for isntallations under path */
    var firstPath= get_installation_path();
    var services            = {};
    var marked_required     = [];
    var modal      = document.querySelector('#modal-install');
    var checkboxes = document.querySelectorAll('.service__action--grid [type=checkbox]');
    var button     = document.querySelector('.services-button')
    checkboxes.forEach(
        function(checkbox){
            checkbox.addEventListener('click', function(){
                // Get checked value of the checkbox
                services[checkbox.getAttribute('name')] = checkbox.checked;
                // Is is there any service to be enabled/disabled the modal is 'active'
                if(Object.values(services).indexOf(true) > -1){
                    modal.classList.add('active');
                } else {
                    modal.classList.remove('active');
                }
                var dependencies = document.querySelectorAll('div[data-service="' + checkbox.dataset.show + '"]');
                    dependencies.forEach(function(founditem) {
                    founditem.classList.toggle('active'); 
                });
            });
        }
    );

    // Validation
    var launch_modal = document.querySelector('.services-button');
    launch_modal.addEventListener("click", function(event){
         event.preventDefault()
    var elements = document.getElementsByClassName("error");
    while(elements.length > 0){
        elements[0].parentNode.removeChild(elements[0]);
      }
        var is_validated          = true;
        var errors                = [];
        marked_services     = [];
        marked_dependencies = [];
        marked_system = [];
        marked_deps_indupt = [];
        var modal_services_list = document.querySelector(".modal__text--install ul");
        while (modal_services_list.firstChild)
            modal_services_list.removeChild(modal_services_list.firstChild);
        for(i in services){
            if(services[i]){
                // Create a list of services to be installed
                marked_services.push(i)
                x=document.querySelector("#"+ i +".service--grid h5").textContent;
                if (x) i=x;
                var li = htmlToElement('<li><i class="fa fa-plus"></i>' + i + '</li>');
                modal_services_list.appendChild(li);
            }
        }

        marked_services.forEach( function(service){
            /* get required group and add them to services to be included in installation*/
            var dep_groups = document.querySelectorAll('input.dependency_'+service+'_group');
            dep_groups.forEach( function(dep_group){
              var name = dep_group.value;
              if (name)
                marked_dependencies.push(name)
                var li = htmlToElement('<li><i class="fa fa-plus"></i>' + name + ' (Dependency)</li>');
                modal_services_list.appendChild(li);
            });
          
            /* fetch other dependency to be checked */
            var deps_system = document.querySelectorAll('input.dependency_'+service+'_system');
            deps_system.forEach( function(dep_system){
              var name = dep_system.getAttribute('name');
              var value = dep_system.value;
              if (value){
                    marked_system.push({
                        'service' : service,
                        'name'    : name,
                        'value'   : value,
                    })
              }

             });
            /* Check user input dependecies */
            var deps_input   = document.querySelectorAll('input[data-service='+service+']');
            deps_input.forEach( function(dep){
                var name  = dep.getAttribute('name');
                var value = dep.value;
                if(value){
                    marked_deps_indupt.push({
                        'service' : service,
                        'name'    : name,
                        'value'   : value,
                    })
                } else {
                    is_validated = false;
                    errors.push({
                        "trigger" : dep,
                        "msg"     : "Necesitas terminar de configurar el servicio " + service,
                    });
                }
            });
        });
        if(is_validated){
          if (marked_system.length > 0 || marked_deps_indupt.length > 0){     
            var data = JSON.stringify({
              'marked_system' : marked_system,
              'marked_deps_indupt' : marked_deps_indupt
            })
            launchModal('#modal-loading');
           // var url = firstPath[0] + 'services/checkinstall';
            var has_errors = false;
            result = postNoReload(firstPath + '/services/checkinstall', data, function(responseText){
          });
            dismiss('#modal-loading');
          }
            json = JSON.parse(result);
            if (json["errors"] == 0){
              launchModal('#modal-install');
            } else {
              has_errors = true;
              /* show errors to be fixed in DOM */
              for (var item =0; item < json["app"].length; item++) { 
                var deps_div  = document.querySelector("#" + json["app"][item].service +".service--grid .service__dependencies--grid");
                var error_div = htmlToElement('<div class="error"><span>' + json["app"][item].errormsg + '</span></div>');
                deps_div.appendChild(error_div); 
              }
            } 
            if (has_errors){
            
                var msg  = "<li class='messages-list__msg--error'>" + json['errorsmsg'] + "</li>";
                var msgs = document.querySelector('.messages-list');
                msgs.appendChild( htmlToElement(msg))
            }

            //launchModal('#modal-install')
        } else {
            // Close modal
            errors.forEach( function(error){
                // Show errors
                var msg  = "<li class='messages-list__msg--error'>" + error.msg + "</li>";
                var msgs = document.querySelector('.messages-list');
                msgs.appendChild( htmlToElement(msg))
                // Mark input as invalid
                error.trigger.setCustomValidity("Introduce un texto");
            })
        }
        console.log(marked_dependencies);
        console.log(marked_services);
    });
    // Submit
    var install_button = document.querySelector('.modal__submit--install');
    install_button.addEventListener("click", function(){
        // Put together servcies and dependecies, as gorups to be imtalled
        //marked_services.push(marked_dependencies)
        install_services= marked_services.concat(marked_dependencies);
        console.log("install_services " + install_services);
        // See /static/mxcp/main.js
        post(firstPath + '/services/install', JSON.stringify({
            'services'     : install_services,
            'marked_deps_indupt' : marked_deps_indupt
            
        }));
    });
});
