var loginform = document.getElementById("form-ready");
var updatingform =  document.getElementById("form-updating");
/*
At the beginiing, the login form is invisible. Till a check against cpanel status
is performed end returns 'ready'
*/
//loginform.className += "hide";
/* TODO: Cache is avoiding getting network error when apache restarts
  Try with  ServerXmlHttpRequest: https://stackoverflow.com/questions/5235464/how-to-make-microsoft-xmlhttprequest-honor-cache-control-directive
  IXMLHTTPRequest http = CreateComObject("Msxml2.ServerXMLHTTP");
  http.open("GET", "http://www.bankofcanada.ca/stat/fx-xml.xml", False, "", "");
  http.setRequestHeader("Cache-Control", "max-age=0");
  http.send();
*/
var firstPath= get_installation_path();
var firstload = true;
var theUrl = firstPath + '/status-cpanel'



function httpGetAsync(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4){
            if (xmlHttp.responseText == 'ready' && xmlHttp.status == 200){
              loginform.classList.toggle("hide");
              updatingform.className += " hide";
            }else if (xmlHttp.responseText != 'ready' && xmlHttp.status == 200){
              // If status not ready, go on with request
              if (firstload == true) {
                updatingform.classList.toggle("hide"); 
                firstload = false;
              }
              setTimeout(httpGetAsync(theUrl), 3000);
            }
            console.log( xmlHttp.responseText );
        }
    }
    xmlHttp.open("POST", theUrl, true); // true for asynchronous 
    var csrftoken = getCookie('csrfcpaneltoken');
    xmlHttp.setRequestHeader("X-CSRFToken", csrftoken);
    xmlHttp.send();

}


httpGetAsync (theUrl);

