// Equivalent to jquery's "$(document).ready()"
document.addEventListener("DOMContentLoaded", function()
{
    var services   = {
        'enable': {},
        'disable' : {}
    };
    var modal      = document.querySelector('#modal-services');
    var checkboxes = document.querySelectorAll('.service__action--grid [type=checkbox]');
    var button     = document.querySelector('.services-button')
    checkboxes.forEach(
        function(checkbox){
            checkbox.addEventListener('click', function(){
                // Get checked value of the checkbox
                services[checkbox.dataset.action][checkbox.getAttribute('name')] = checkbox.checked;
                // Is is there any service to be enabled/disabled the modal is 'active'
                if(Object.values(services['enable']).indexOf(true) > -1 || Object.values(services['disable']).indexOf(true) > -1){
                    modal.classList.add('active');
                } else {
                    modal.classList.remove('active');
                }
                // Update in the modal the list of services to be disabled
                var modal_services_list = document.querySelector(".modal__text--services ul");
                while (modal_services_list.firstChild)
                    modal_services_list.removeChild(modal_services_list.firstChild);
                for(i in services['enable']){
                    if(services['enable'][i]){
                        var li = htmlToElement('<li><i class="fa fa-plus"></i>' + i + '</li>');
                        modal_services_list.appendChild(li);
                    }
                }
                for(i in services['disable']){
                    if(services['disable'][i]){
                        var li = htmlToElement('<li><i class="fa fa-close"></i>' + i + '</li>');
                        modal_services_list.appendChild(li);
                    }
                }
            });
        }
    );
    // Submit
    var submit = document.querySelector('.modal__submit--services');
    submit.addEventListener("click", function(){
        // See /static/mxcp/main.js
        var firstPath= get_installation_path();
        post( firstPath + '/services/update', JSON.stringify(services))
    });
});
