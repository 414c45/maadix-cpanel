var loginform = document.getElementById("form-ready");
var updatingform =  document.getElementById("form-updating");
/*
At the beginiing, the login form is invisible. Till a check against cpanel status
is performed end returns 'ready'
*/
//loginform.className += "hide";
var firstload=true;
var firstPath= get_installation_path();
var theUrl = firstPath + '/status-cpanel';
var oldDomain= getCookie('oldDomain');
var newDomain= getCookie('newDomain');
var theNewUrl = "https://" + newDomain + theUrl;
function httpGetAsync(theUrl)
{
    try
    {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4){
            /* Check if a result is given to avoid json error if no value is rtutned*/
            var response = (xmlHttp.responseText)
            if (xmlHttp.status == 200 && response == 'ready'){
              loginform.classList.toggle("hide");
              updatingform.className += " hide";
              
            }else if (xmlHttp.status == 200 && response != 'ready'){
              // If status not ready, go on with request
              if (firstload == true) {
                updatingform.classList.toggle("hide"); 
                firstload = false;
              }
              setTimeout(httpGetAsync(theUrl), 3000);


            } else {
                theUrl=theNewUrl;
                //var xmlHttp = new XMLHttpRequest();
                xmlHttp.open("POST", theUrl, true);
                xmlHttp.setRequestHeader("X-CSRFToken", csrftoken);
                setTimeout(httpGetAsync(theUrl), 3000);
            }   

        } 
          
    }
    xmlHttp.open("POST", theUrl, true); // true for asynchronous 
    var csrftoken = getCookie('csrfcpaneltoken');
    xmlHttp.setRequestHeader("X-CSRFToken", csrftoken);
    xmlHttp.send();
    }
    catch(err){

        console.log("cat error" + err.message);
        setTimeout(httpGetAsync(theUrl), 5000);
    }

}


httpGetAsync (theUrl);
