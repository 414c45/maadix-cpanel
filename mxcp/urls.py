# django
from django.contrib import admin
from django.conf.urls.i18n import i18n_patterns
from django.urls import path, re_path, include
from django.views.generic.base import TemplateView
from django.contrib.auth import views as auth_views
from django.conf.urls import i18n 
# project
from apps.views import views_activate as activate
from apps.views import views_system as system
from apps.views import views_domains as domains
from apps.views import views_dns as dns
from apps.views import views_mails as mails
from apps.views import views_notifications as notifications
from apps.views import views_services as services
from apps.views import views_users as users
from apps.views import view_delete as delete
from apps.views import views_applications as applications
from apps.views import view_status as status


urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    # Activation (first access)
    path('activate', activate.Activate.as_view(), name="activate"),
    # registration
    path('profile', users.Profile.as_view(), name="profile"),
    path('login', users.LoginView.as_view(), name="login"),
    path('changesystem', system.NewFqdnSet.as_view(), name="changesystem"),
    path('password-recovery', users.PasswordRecovery.as_view(), name="password-recovery"),
    path('password-reset', users.PasswordReset.as_view(), name="password-reset"),
    #path('logout', auth_views.logout, name="logout"),
    path('logout', users.LogoutView.as_view(), name="logout"),

    # sections
    path('', system.Details.as_view(), name="system-details"),
    path('system/reboot', system.Reboot.as_view(), name="reboot"),
    path('system/update', system.Update.as_view(), name="update"),
    path('system/fqdn', system.Fqdn.as_view(), name="fqdn"), 
    path('services', services.Services.as_view(), name="services"),
    # a link to services-update is harcoded inside static/mxcp/js/services-update.js
    path('services/update', services.ServicesUpdate.as_view(), name="services-update"),
    path('install', services.ServicesAvailable.as_view(), name="services-available"),
    # a link to services-install is harcoded inside static/mxcp/js/services-install.js
    path('services/install', services.ServicesInstall.as_view(), name="services-install"),
    path('services/checkinstall', services.ServicesCheckinstall.as_view(), name="services-checkinstall"),
    path('services/settings/<str:appid>/', applications.AppSettings.as_view(), name='apps-settings'),
    path('domains', domains.DomainsListView.as_view(), name="domains"),
    path('domains/add', domains.AddDomainView.as_view(), name="domains-add"),
    path('domains/edit', domains.EditDomainView.as_view(), name="domains-edit"),
    path('domains/instructions', TemplateView.as_view(template_name='pages/domains-instructions.html'), name="domains-instructions"),

    path('mailman/domains', domains.MailmanDomains.as_view(), name="domains-mailman"),
    path('dns', dns.DnsView.as_view(), name="dns"),

    path('mails', mails.MailAccounts.as_view(), name="mails"),
    path('email/', mails.MailAccount.as_view(), name="email"),

    path('users', users.Users.as_view(), name="users"),
    path('users/edit', users.User.as_view(), name="user"),
    path('users/superuser/edit', users.SuperUser.as_view(), name="superuser"),
    path('users/postmasters', users.Postmasters.as_view(), name="postmasters"),
    path('users/postmasters/edit', users.Postmaster.as_view(), name="postmaster"),

    path('notifications', notifications.Notifications.as_view(), name="notifications"),

    # api
    path('api/delete-entry', delete.DeleteEntry.as_view(), name="api_delete"),
    path('api/get-cpu-usage', system.get_cpu_usage, name="api_get_cpu"),
    path('api/create-dkim', domains.CreateDkim.as_view(), name="api_create_dkim"),

    #ldap cpanel status
    # a link to services-install is harcoded inside static/mxcp/js/status.js
    path('status-cpanel', status.CheckCpanelStatus.as_view(), name="status-cpanel")
    ]
