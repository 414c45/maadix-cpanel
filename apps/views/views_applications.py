# python
import os, re
# django
#from django import views
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic.edit import FormView
from django.urls import reverse_lazy, reverse
from django.utils.encoding import force_bytes
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
# contrib
from ldap3 import HASHED_SALTED_SHA
from ldap3.utils.hashed import hashed
from ldap3 import MODIFY_REPLACE
# project
from .utils import get_release_info, connect_ldap, get_puppet_status, p, lock_cpanel, get_server_ip, get_server_host, domain_is_in_use, get_existing_domains, get_input_deps_fields, ldap_val, check_domain_avalability
from .forms import EditAppForm 
from django.conf import settings


class AppSettings(FormView):
    """
    Edit applications setting for dependencies 
    """
    template_name = 'pages/apps.html'
    form_class    = EditAppForm
    ldap          = {}
    ldap_attributes = ['status']
    dep_status    = {}
    inputdeps     = []
    maintenance   = False
    appid=None

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get(self, request, **kwargs):
        self.appid = self.kwargs.get('appid', None)
        data = get_input_deps_fields(request, self.appid)
        self.inputdeps = data['inputdeps']
        self.maintenance = data['maintenance']
        appname = data['appname'] 
        self.appname = appname[0]
        return super(AppSettings, self).get(request)

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context = super(AppSettings, self).get_context_data(**kwargs)
        context['appname'] = self.appname 
        context['maintenance'] = self.maintenance
        return context

    def get_form_kwargs(self):
        """Return the keyword arguments for instantiating the form."""
        kwargs = super().get_form_kwargs()
        default_values = self.dep_status
        kwargs['initial'] = default_values
        
        return kwargs


    def get_form(self):
        """Return an instance of the form to be used in this view."""
        try:
            """ Populate dynamic dependencies form fields with vañues from ldap """
            for deps in self.inputdeps:
                # Get a lis of all input fields
                print('dep: %s' %  deps['id'])
                dn = 'ou=%s,ou=%s,%s' % (deps['id'], self.appid, settings.LDAP_TREE_SERVICES);
                self.request.ldap.search(
                    dn,
                    "(objectclass=*)",
                    attributes = self.ldap_attributes
                )

                self.dep_status['%s' % deps['id']] = self.request.ldap.entries[0]['status']
                #self.cleaned_data['%s' % deps['id']] = self.request.ldap.entries[0]['status']
        except Exception as e:
            print(e)
        return EditAppForm(inputdeps=self.inputdeps,maintenance=self.maintenance, appid=self.appid, **self.get_form_kwargs())

    def post(self, request, *args, **kwargs):
        fields=[]
        data = self.get(request)
        has_changed = False
        print ('SATA %s: ' % self.inputdeps)
        # get all input fields name
        for field in self.inputdeps:
            fields.append(field['id'])
        if 'domain' in fields:
            defaults = self.get_form_kwargs()
            print('defaults %s ' % defaults)
            default_domain = defaults['initial']['domain']
            self.domain = self.request.POST['domain']
            # If domain has not changed do net check availability
            if not self.domain == default_domain:
                message = check_domain_avalability(self.request.ldap,self.domain) 
                if message:
                    messages.error(self.request,message)
                    return HttpResponseRedirect( self.request.get_full_path())
            for deps in self.inputdeps:
        
                fieldname = deps['id']
                fieldvalue = self.request.POST[fieldname]
                old_fieldvalue =  defaults['initial'][fieldname]
                if not old_fieldvalue == fieldvalue:
                    has_changed = True
                    # If value has changed update ldap 
                    try:
                        # Update field
                        dn = 'ou=%s,ou=%s,%s' % (fieldname,self.appid, settings.LDAP_TREE_SERVICES)
                        request.ldap.modify(dn, {
                            'status' : [(MODIFY_REPLACE, fieldvalue)]
                        })
                        # If filed is domain, record old value in ldap for puppet to be able to delete old domain
                        if fieldname == 'domain':
                            try: 
                                base_old_dn = 'ou=domain_old,ou=%s,%s'% (self.appid, settings.LDAP_TREE_SERVICES)
                                request.ldap.search(
                                    base_old_dn,
                                    "(&(objectClass=organizationalUnit)(objectClass=metaInfo))",
                                )
                                if request.ldap.entries:
                                    try: 
                                        request.ldap.modify(base_old_dn, {
                                            'status' : [(MODIFY_REPLACE, old_fieldvalue)]
                                        })

                                    except Exception as e:
                                        messages.error(request,_("ha habido un problema actualizando la información"))
                                        p("view_applications.py", "✕ There's was a problem updating old domain value", e)
                       
                            except:
                                old_domain = False
                            if not old_domain:    
                                try: 
                                    request.ldap.add(base_old_dn, [
                                        'organizationalUnit',
                                        'metaInfo',
                                        'top'
                                    ], { 'status' : old_fieldvalue } 
                                    )

                                except Exception as e:
                                    messages.error(request,_("ha habido un problema actualizando la información"))
                                    p("view_applications.py", "✕ There's was a problem creating old domain value", e)
           
                    except Exception as e:
                        messages.error(request,_("ha habido un problema actualizando la información"))
                        p("view_domains.py", "✕ There's was a problem creating the domain",  e)
            if has_changed:
                lock_cpanel(self.request) 
                return HttpResponseRedirect( reverse('logout') )

        return HttpResponseRedirect( self.request.get_full_path())
