# python
import datetime, time, random, os, urllib3, json, shutil, sys, hashlib, socket, re, psutil, binascii
from distutils.dir_util import copy_tree
# django
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import force_bytes
from django.core.management import utils as django_management_utils
from django.core.mail import send_mail, EmailMessage, EmailMultiAlternatives
from django.template.loader import get_template
from django.utils.html import escape
from django.core.cache import cache
from django.core.signing import TimestampSigner
# contrib
import onetimepad
import ldap3
import dns.resolver
from .uniqid import uniqid
import psycopg2
# project
from django.conf import settings

def get_server_host():
    hostname = socket.getfqdn()
    return hostname if hostname != '0' else 'localhost'

def get_server_ip():
    hostname = get_server_host()
    return socket.gethostbyname(hostname)

def get_server_hostname():
    hostname = socket.gethostname()
    return hostname

def p(trigger, txt, exception):
    """ Prints exceptions to console. """
    if settings.DEBUG:
        print("---", trigger, txt, str(exception), "---", sep="\n")

def unix_timestamp():
    """ Returns Unix timestamp """
    return int(round(time.time() * 1000))

def ldap_creation_date():
    """ Returns creation date in the format used by LDAP """
    return datetime.datetime.now().strftime('%Y%m%d')

def ldap_bool(bool):
    """ Convert a boolean value to its string representation in LDAP """
    if bool:
        return 'TRUE'
    return 'FALSE'

def ldap_val(val):
    """ Convert LDAP val to a valid str representation in Django """
    #Modify the logic to use this function also for text attribute that may be true or false (lowercase)
    if (val == 'TRUE' or val == 'True'):
        return True
    elif (val == 'FALSE' or val == 'False'):
        return False
    return val
    """
    if val.lower() == 'true':
        return True
    elif val.lower() == 'false':
        return False
    return val
    """
def md5(str):
    """ Returns the MD5 hash of a given str """
    return hashlib.md5(force_bytes(str)).hexdigest()

def delete_key(user):
    """ Deletes encryption key. """
    filename = '/tmp/%s--%s__key' % (get_server_host(), user)
    if os.path.isfile(filename):
        os.remove(filename)

def gen_pwd():
    """ Generates random password. """
    return django_management_utils.get_random_secret_key()

def dec_pwd(request):
    """ Return decrypted pwd from request data """
    # Use Django file-based  session to store data
    user = request.session['user']
    key = request.session['session_key']
    enc_pass = request.COOKIES.get('s')
    return onetimepad.decrypt(enc_pass, key)

def enc_pwd(pwd):
    """ Encrypt password with one time pad """

    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
    key   = ""
    for i in pwd:
        key += random.choice(chars)
    return onetimepad.encrypt(pwd, key), key

def get_token(length=32):
    """ Creates a token for password recovery """
    """
    # get ASCII numerals + uppercase + lowercase
    chars = [chr(i) for i in range(48, 59)] + [chr(i) for i in range(65, 91)] + [chr(i) for i in range(97, 123)]
    # set a token of the given length concatenating random characters of prev array
    token = ''.join([ random.choice(chars) for i in range(length) ])
    """
    token = os.urandom(32).hex()
    return token

def get_user_role(username):
    return 'admin' if not '@' in username else 'postmaster' if 'postmaster' in username else 'email'

def user_format(username):
    # if @ in the username try to build DN of webmasters or email accounts
    if '@' in username:
        username_components = username.split('@')
        user   = username_components[0]
        domain = username_components[1]
        if user == 'postmaster':
            return 'cn=postmaster,vd=%s,%s' % ( domain, settings.LDAP_TREE_HOSTING )
        return 'mail=%s,vd=%s,%s' % ( username, domain, settings.LDAP_TREE_HOSTING )
    # else try admin dn
    return "cn=%s,%s" % ( username, settings.LDAP_TREE_BASE )

def connect_ldap(username, password):
    """ Connect to LDAP """
    dn = user_format(username)
    try:
        # Configure the connection.
        if settings.LDAP_AUTH_USE_TLS:
            auto_bind = ldap3.AUTO_BIND_TLS_BEFORE_BIND
        else:
            auto_bind = ldap3.AUTO_BIND_NO_TLS
        connection = ldap3.Connection(
            ldap3.Server(
                settings.LDAP_AUTH_URL,
                allowed_referral_hosts = [("*", True)],
                get_info               = ldap3.NONE,
                connect_timeout        = settings.LDAP_AUTH_CONNECT_TIMEOUT,
            ),
            user             = dn,
            password         = password,
            auto_bind        = auto_bind,
            raise_exceptions = True,
            receive_timeout  = settings.LDAP_AUTH_RECEIVE_TIMEOUT,
        )
        return {
            'connection' : connection,
        }
    except Exception as e:
        print("There's a problem connecting to LDAP: %s" % str(e))
        return {
            'connection' : None,
            'error'      : e.__class__.__name__
        }
def recursive_delete(ldap, base_dn):
    #TODO: Check if there is a better way, to avoid looping if not needed
    # Right now only vd=domain need recursive deletion
    # Maybe use this function only for domains, or leave it for all deletion for future cases 
    try:
        ldap.search(base_dn,
            '(objectClass=*)',
        )
        search = ldap.entries
        print(search)
    except Exception as e:
        p("utils.py · recursive_delete", "✕ There was a problem retrieving the itme", e)

    for dn in search:
        item = dn.entry_dn
        if (item not in base_dn):
            print('delete %s' % item)
            try:
                ldap.delete(item)
            except Exception as e:
                p("utils.py · recursive_delete", "✕ There was a problem deleting  item", e)
                raise 
    ldap.delete(base_dn)

    """ If we are deleting a user, check if it is in web group.
        If so remove it 
    """
    if ('ou=sshd,ou=People,' in base_dn):
        # the dn is a user
        split_dn = base_dn.split(",")
        split_dn_uid=split_dn[0]
        user_dn= split_dn_uid.split("=")
        is_uid = user_dn[0]
        username = user_dn[1]
        if (is_uid == 'uid' and username):
            """ search this iser in web group """
            webgorup_dn = 'cn=web,ou=groups,ou=People,%s' % settings.LDAP_TREE_BASE
            try:
                ldap.search( webgorup_dn,
                    '(memberuid=%s)'% username,
                )
                web_user = ldap.entries
                print("web user is: ",web_user)
                if (web_user):
                    attr = {'memberuid' : [(ldap3.MODIFY_DELETE, username)]} 
                    try:
                        ldap.modify(webgorup_dn, attr)    
                    except Exception as e:
                        p("✕ view_users.py", "There was a problem updating the web group for user %s :" % user, e)

            except Exception as e:
                p("utils.py · recursive_delete", "✕ There was a problem retrieving the item from users group", e)
def find_procs_by_name(name):
    "Return a list of processes matching 'name'."
    exists = False 
    for p in psutil.process_iter(attrs=['name']):
        print("process is ", p )
        if p.info['name'] == name:
            exists = True 
    return exists

def is_ervice_running(process_name):
    """
    Check if a local Apache instance is running and listening to certain ports.
    :param ports: List of ports to listen (all must be on)
    """

    ports_to_go = list(ports)

    for proc in psutil.process_iter():
        if proc.name != process_name:
            # Not target
            continue

        for con in proc.get_connections():
            # Tuple ip, port
            port = con.local_address[1]
            if port in ports_to_go:
                ports_to_go.remove(port)

    return len(ports_to_go) == 0
    
def anonymous_connect_ldap():
    """ Connect to LDAP as anonymous user. """

    try:
        # Configure the connection.
        if settings.LDAP_AUTH_USE_TLS:
            auto_bind = ldap3.AUTO_BIND_TLS_BEFORE_BIND
        else:
            auto_bind = ldap3.AUTO_BIND_NO_TLS
        c = ldap3.Connection(
            ldap3.Server(
                settings.LDAP_AUTH_URL,
                allowed_referral_hosts=[("*", True)],
                get_info=ldap3.NONE,
                connect_timeout=settings.LDAP_AUTH_CONNECT_TIMEOUT,
            ),
            auto_bind=auto_bind,
            raise_exceptions=True,
            receive_timeout=settings.LDAP_AUTH_RECEIVE_TIMEOUT,
        )
        return c
    except Exception as e:
        print("There's a problem connecting to LDAP: %s" % str(e))
        return None

def get_puppet_status(request):
    """ Connect to API and get Maadix release information """

    # Look for services
    try:
        request.ldap.search(
            settings.LDAP_TREE_API,
            '(objectClass=*)',
            attributes=['uid', 'userpassword', 'pass', 'host']
        )
        credentials = request.ldap.entries[0]
        try:
            url = credentials.host.value + 'vmstatus/' + get_server_hostname()
            if settings.DEBUG:
                urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
            http = urllib3.PoolManager()
            # Api call for puppetstatus must be GET
            response = http.request('GET', url,
                headers={
                    'Content-Type' : 'application/json',
                    'X-Auth-Token' : credentials.userpassword.value,
                    'X-User-Id'    : credentials.uid.value
                }
            )
            release_info = response.data.decode('utf-8')
            return json.loads(release_info)
        except Exception as e:
            p("utils.py", "✕- get_puppet_status- There's a problem with the HTTP Request to API endpoint %s" % url, e)
            return None
    except Exception as e:
        p("utils.py", "✕ There's a problem connecting with LDAP", e)
        return None

def get_cpanel_status(ldap):
    """ If cpanel is locked user can't log in"""
    """ This can be an anonumous ldap connection """
    try:
        ldap.search(
            settings.LDAP_TREE_CPANEL,
            '(ou=cpanel)',
            attributes=['status']
        )
        status = ldap.entries[0]['status']
        return status
    except Exception as e:
        p("utils.py", "✕ There's a problem connecting with LDAP", e)
        return None


def lock_cpanel(request):
    """ Locks CPanel """

    try:
        # lock cpanel
        request.ldap.modify(
            settings.LDAP_TREE_CPANEL, {
                'status' : [(ldap3.MODIFY_REPLACE, 'locked')]
            }
        )
    except Exception as e:
        p('utils.py', "✕ There was a problem locking cpanel", e)


def get_release_info(request, route=None):
    """ Connect to API and get Maadix release information """

    # Look for services
    try:
        request.ldap.search(
            settings.LDAP_TREE_CPANEL,
            '(objectClass=*)',
            attributes=['type']
        )
        release = request.ldap.entries[0].type.value
        request.ldap.search(
            'ou=api,dc=example,dc=tld',
            '(objectClass=*)',
            attributes=['uid', 'userpassword', 'pass', 'host']
        )
        credentials = request.ldap.entries[0]
        try:
            #url = credentials.host.value + 'release' if not route else route
            endpoint = 'release' if route==None else route
            url = credentials.host.value + endpoint 
            if settings.DEBUG:
                urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
            http = urllib3.PoolManager()
            response = http.request('POST', url,
                body = json.dumps({
                    'vmname'  : get_server_hostname(),
                    'release' : release,
                }).encode('utf-8'),
                headers={
                    'Content-Type' : 'application/json',
                    'X-Auth-Token' : credentials.userpassword.value,
                    'X-User-Id'    : credentials.uid.value,
                }
            )
            release_info = response.data.decode('utf-8')
            return json.loads(release_info)
        except Exception as e:
            if settings.DEBUG:
                print("get_release_info There's a problem with the HTTP Request to API endpoint %s or no updates available:" % url)
                print(e)
            return None
    except Exception as e:
        print("There's a problem connecting to LDAP:")
        print(e)

def check_value(query, values):
    if values:
        return values == query or query in values
    return False

def check_installed_service(service):
    """ Check if a service is installed in the Maadix instance """
    pass

def get_dns_records(domain):
    """ Retrieve dns info using dns-python """
    answers        = { }
    for record in ['A', 'MX', 'NS', 'TXT']:
        try:
            answers[record] = dns.resolver.query(domain, record)
        except Exception as e:
            print(str(e))
    return answers

def check_dns_A_record(domain_records):
    MESSAGES = { 
        'NOT_A_RECORD'   : _("No se ha encontrado ningún registro para el dominio."),
        'A_RECORDS'      : _("El dominio tiene configurado más de un registro de tipo A. "
                             "Esta configuración puede provocar anomalías. A menos que sepas "
                             "exactamente lo que estás haciendo es aconsejable que dejes un "
                             "solo registro."),
        'OK'             : _("La configuración de DNS para el servidor web es correcta"),
        'NOT_OK'         : _("La configuración de los DNS no es correcta para el servidor web. "
                             "Sigue las instrucciones a continuación para corregirla"),
        'NOT_MAILSERVER' : _("El servidor de correo no está activado para este dominio. "
                             "En el caso quisieras activarlo la siguiente tabla te muestra los valores "
                             "DNS correctos."),
        #'CHANGE_IP'      : _("Edita el registro de tipo A cambiando la actual IP %s por %s"),
    } 
    ip              = get_server_ip()
    msg             = '' 
    has_A           = 'A' in domain_records
    record          = domain_records['A'][0] if has_A else None
    records_A       = domain_records['A'] if has_A else [None]

    error           = True
    results         = {}
    if not has_A:
        msg = MESSAGES['NOT_A_RECORD']
    else:
        A_records_number = len(domain_records['A'])
        if  A_records_number == 1 and '%s' % record == '%s' % ip: 
            msg = MESSAGES['OK']
            error = False
        elif A_records_number > 1 and ip in records_A:
            msg = MESSAGES['A_RECORDS']
        else:
            msg = MESSAGES['NOT_OK']
    results = { 'msg': msg, 'error' : error }
    return results

def get_dkim_status_cpanel(ldap):
    """ Check If cpaneldkim_status is locked """
    try:
        ldap.search(
            settings.LDAP_TREE_DKIM,
            settings.LDAP_FILTERS_DKIM,
            attributes=['status']
        )
        status = ldap.entries[0]['status']
        return status

    except Exception as e:
        p("utils.py", "✕ There's a problem connecting with LDAP", e)
        return None


def get_dkim(domain):
    """ Retrieve DKIM info using dns-python """
    domain = "default._domainkey.%s" % domain
    try:
        answer = dns.resolver.query(domain, 'TXT')
        return answer.to_text()
    except Exception as e:
        p("utils.py · get_dkim", "✕ There's a problem checking the DKIM register of the domain %s:" % domain, e)
    return None


def lock_dkim(ldap):
    """ Lock DKIM status """

    ldap.modify(settings.LDAP_TREE_DKIM, {
        'status'       : [( ldap3.MODIFY_REPLACE, 'locked' )],
    })


def add_dkim(ldap, domain):
    """ Add DKIM record to domain """

    dkim = ldap.search(
        settings.LDAP_TREE_DKIM,
        settings.LDAP_FILTERS_DKIM,
        attributes=['cn']
    )
    if not dkim:
        ldap.add(settings.LDAP_TREE_DKIM, [
            'organizationalUnit',
            'top',
            'metaInfo',
        ])
    ldap.add("ou=%s,%s" % (domain, settings.LDAP_TREE_DKIM), [
        'organizationalUnit',
        'top',
    ])
    lock_dkim(ldap)


def remove_dkim(ldap, domain):
    """ Remove DKIM record related to a domain """

    dn = "ou=%s,%s" % (domain, settings.LDAP_TREE_DKIM)
    ldap.delete(dn)
    lock_dkim(ldap)

def check_mail_active(ldap, domain):
    """ Check if mail is active for a given domain """

    try:
        ldap.search(
            settings.LDAP_TREE_BASE,
            "(vd=%s)" % domain,
            attributes=['accountActive']
        )
        return ldap.entries[0].accountactive.value
    except Exception as e:
        p("utils.py · check_mail_active", "✕ There's a problem checking the mail system of the domain %s:" % domain, e)
        return False

def domain_pointed_to(domain, ip):
    """ Checks if a domain is pointed to a given IP """

    try:
        domain_ip = dns.resolver.query(domain, 'A')
        return domain_ip == ip
    except Exception as e:
        p("utils.py · domain_pointed_to", "✕ There's a problem checking the A register of the domain %s:" % domain, e)
    return None


def check_rainloop_conf(domain):
    """ Checks if there's Rainloop conf for a given domain and creates it if not. """

    rainloop_domain_conf = '/var/www/rainloop/data/_data_/_default_/domains/%s.ini' % domain
    if not os.path.isfile(rainloop_domain_conf):
        try:
            with open(rainloop_domain_conf, "w") as conf_file:
                conf_file.write('\n'.join([
                    'imap_host ="%s"' % get_server_host(),
                    'imap_port = 143',
                    'imap_secure = "TLS"',
                    'imap_short_login = Off',
                    'sieve_use = Off',
                    'sieve_allow_raw = Off',
                    'sieve_host = ""',
                    'sieve_port = 4190',
                    'sieve_secure = "None"',
                    'smtp_host = "%s"' % get_server_host(),
                    'smtp_port = 25',
                    'smtp_secure = "TLS"',
                    'smtp_short_login = Off',
                    'smtp_auth = On',
                    'smtp_php_mail = Off',
                    'white_list = ""',
                ]))
        except Exception as e:
            p("utils.py · create_rainloop_conf", "✕ There's a problem creating Rainloop conf file for domain %s:" % domain, e)


def notify_email(email):
    """ Notifies user about his new email. """

    try:
        body = _("Bienvenido a tu nuevo buzón. Por favor no contestes a este mensaje")
        send_mail(
            'Bienvenido',
            body,
            'no-reply@%s' % get_server_host(),
            [email],
            fail_silently=False,
        )
    except Exception as e:
        p("utils.py · notificate_email", "✕ There's a problem notifying new email " % email, e)

def send_vpn_instructions(request, email, username):
    """ Sends by email VPN instructions """

    try:
        # from
        request.ldap.search(
            settings.LDAP_TREE_SENDERMAIL,
            settings.LDAP_FILTERS_SENDERMAIL,
            attributes=['cn']
        )

        # body
        host = get_server_host()
        body =  _("Hola."
                 "El administrador de %s ha activado un acceso VPN para tu cuenta. "
                 "Usuario: %s"
                 "Contraseña: Por razones de seguridad, no se envían contraseñas por correo electrónicoi. "
                 "Debes solicitarla al administrador. "
                 "Por favor, descarga el archivo adjunto y sigue las instrucciones "
                 "para tu sistema operativo, disponibles en: "
                 "http://docs.maadix.net/vpn/" % host, username)
        host = get_server_host()
        html_body = _("<p>Hola.</p><p>El administrador de %s ha activado un acceso VPN para tu cuenta de usuario</p>"
                 "<p><b>Usuario</b>: %s</p>"
                 "<p><b>Contraseña</b>: <em>Por razones de seguridad, no se envían contraseñas por correo electrónico. "
                 "Debes solicitarla al administrador</em></p>"
                 "<p>Descarga el archivo adjunto y sigue las instrucciones "
                 "para tu sistema operativo, disponibles en:</p>"
                 "<p><a href='http://docs.maadix.net/vpn'>http://docs.maadix.net/vpn/</a></p>" % host, username)
        # attachments
        filesdir   = '/tmp/vpn_files'
        foldername = 'VPN-%s' % get_server_ip()
        folderpath = '%s/%s'  % (filesdir, foldername)
        filepath   = '%s' % folderpath
        if not os.path.isdir(filesdir):
            os.mkdir(filesdir)
        """
        if not os.path.isdir(folderpath):
            os.mkdir(folderpath)
        """
        crt     = '/etc/openvpn/ca.crt'
        tmp_crt = '%s/ca.crt' % filesdir
        if os.path.isfile(crt):
            shutil.copyfile(crt, tmp_crt)
        vpn_config_files = '%s/files/vpn_config' % settings.BASE_DIR
        # copy_tre problem with cache. It only copy once
        # https://bugs.python.org/issue10948
        #copy_tree(vpn_config_files, folderpath)
        shutil.copytree (vpn_config_files, folderpath)
        for root, dirs, files in os.walk(folderpath):
            for subfolder in dirs:
                # Override crt each time.  It may have changed
                shutil.copy(crt, os.path.join(root, subfolder, 'ca.crt'))
        conf_files = [
            '%s/linux/vpn.conf'              % folderpath,
            '%s/windows/vpn.ovpn'            % folderpath,
            '%s/android/android-client.ovpn' % folderpath,
        ]
        for conf_file in conf_files:
            with open(conf_file, 'a') as f:
                print('copiando en %s' % f)
                f.write('remote %s' % get_server_ip())
                f.close()
        shutil.make_archive(filepath, 'zip', folderpath)
        attachment_file = filepath + '.zip'
        # send email
        #to = request.ldap.entries[0].cn.value if request.ldap.entries else 'www-data@%s' % get_server_host()
        to = email
        sender = get_notifications_address(request.ldap)
        sendmail = EmailMultiAlternatives('Cuenta VPN activada', body, sender, [to])
        sendmail.attach_alternative(html_body, "text/html")
        print('attachment %s' % attachment_file)
        if os.path.isfile(attachment_file):
            print('attachment %s' % attachment_file)
            sendmail.attach_file(attachment_file)
        try:
            sendmail.send(fail_silently=False)
            shutil.rmtree(filesdir)
            # Remove von temporary files. If IP or ca changes we nedd to regenrate them
            # Build the .zip folder each time
            #shutil.rmtree(filesdir, ignore_errors=True)
        except Exception as e:
            p("utils.py · send_vpn_instructions", "✕ There's a problem sending new email %s" % sendmail, e)
    except Exception as e:
        p("utils.py · send_vpn_instructions", "✕ There's a problem notifying new email %s" % email, e)

def get_domain(ldap, domain_name):
    """ Get LDAP record of a given domain """

    try:
        ldap.search(
            'o=hosting,dc=example,dc=tld',
            '(vd=%s)' % domain_name,
            attributes    = ['mail', 'adminid', 'accountActive']
        )
        return ldap.entries[0]
    except Exception as e:
        p("utils.py · get_domain", "✕ There was a problem retrieving the domain: " % domain_name, e)

def get_users(ldap):
    """ Get LDAP users """

    try:
        ldap.search(
            settings.LDAP_TREE_USERS,
            settings.LDAP_FILTERS_USERS,
            attributes = ['uid', 'gidnumber']
        )
        return [ user.uid.value for user in ldap.entries ]
    except Exception as e:
        p("utils.py · get_users", "✕ There was a problem retrieving the user list", e)


def has_dkim(ldap, domain_name):
    """ Get DKIM record of a given domain """

    try:
        return ldap.search(
            'ou=%s,ou=opendkim,ou=cpanel,%s' % ( domain_name, settings.LDAP_TREE_BASE ),
            '(objectClass=organizationalUnit)',
        )
    except Exception as e:
        p("utils.py · has_dkim", "✕ There was a problem retrieving the DKIM", e)


def get_existing_emails(ldap, domain=None):
    """ Get a list of existing emails in LDAP. """

    base    = settings.LDAP_TREE_HOSTING
    if domain:
        base = 'vd=%s,%s' % ( domain, base )
    try:
        ldap.search(
            base,
            settings.LDAP_FILTERS_EMAILS,
            attributes    = ['mail', 'cn']
        )
        return ldap.entries
    except Exception as e:
        p("utils.py · get_existing_emails", "✕ There was a problem getting list of emails", e)

    return []


def get_existing_domains(ldap):
    """ Get a list of existing domains in LDAP. """

    try:
        ldap.search(
            settings.LDAP_TREE_HOSTING,
            settings.LDAP_FILTERS_DOMAINS,
            attributes=['vd', 'accountActive', 'adminId']
        )
        return ldap.entries
    except Exception as e:
        p("utils.py · get_existing_domains", "✕ There was a problem retrieving the domain list", e)
        return []

def domain_is_in_use(ldap):
    """ Check ig a given domain is in use by some application """

    try:
        ldap.search(
            settings.LDAP_TREE_SERVICES,
            "(&(objectClass=organizationalUnit)(ou=domain)(status=*))", 
            attributes=['status']
        )
        return ldap.entries
        print(ldap.entries)
    except Exception as e:
        p("utils.py · domain_is_in_use", "✕ There was a problem retrieving the domain list", e)
        return []

def check_domain_avalability(ldap,domain):
    message = ''
    domains = get_existing_domains(ldap)
    domain_list = [ domain.vd.value for domain in domains ]
    domains_in_use = domain_is_in_use(ldap)
    domains_used_list = [ domain.status.value for domain in domains_in_use]
    if not re.match("^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,20}$", domain):
        message = _("%s no es un nombre de dominio válido" % domain)
    elif domain in domain_list:
        message =_("El dominio %s ya está activado" % domain)
    elif domain in get_server_host():
        message =_("No puedes añadir el dominio actual del cpanel")
    elif domain in domains_used_list:
        message = _("El dominio %s está en uso por otra aplicación" % domain)
    else: 
        dns_records = get_dns_records(domain)
        record_a = check_dns_A_record(dns_records)
        if record_a['error']:
            message = _("La configuración de los DNS para El dominio %s no es correcta." % domain)
    return message 

def get_notifications_address(ldap):
    """ Get email address that sends notifications. """

    try:
        ldap.search(
            search_base   = settings.LDAP_TREE_SENDERMAIL,
            search_filter = settings.LDAP_FILTERS_SENDERMAIL,
            attributes    = ['cn']
        )
        return ldap.entries[0].cn.value
    except Exception as e:
        p("utils.py · get_notifications_address", "✕ There was a problem retrieving the sendermail", e)
        if e.result == 32:
            ldap.add(settings.LDAP_TREE_SENDERMAIL, [
                'organizationalUnit',
                'top',
                'metaInfo'
            ])
        return 'www-data@%s' % get_server_host()

def get_user_domains(ldap, user):
    """ Get a list of domains administrated by a user. """

    try:
        ldap.search(
            search_base   = settings.LDAP_TREE_BASE,
            search_filter = "(&(vd=*)(adminid=%s))" % user,
            attributes    = ['vd']
        )
        domains = [ entry.vd.value for entry in ldap.entries ]
        return domains
    except Exception as e:
        p("utils.py · get_user_domains", "✕ There was a problem getting domain list for user %s" % user, e)

    return []

def get_admin(ldap):
    """ Get admin data for password recovery. """

    try:
        ldap.search(
            search_base   = settings.LDAP_TREE_BASE,
            search_filter = "(&(objectclass=extensibleObject)(!(cn=uidNext)))",
            attributes    = ['email', 'cn']
        )
        return ldap.entries[0]
    except Exception as e:
        p("utils.py · get_existing_emails", "✕ There was a problem getting superadmin data", e)

    return {}

def password_recover(username, email):
    """ Write a file to store the data needed for password recovery """
    # Additional security check when recover password. Us a signed token + a code
    # that user will have to input into the form
    #code = binascii.hexlify(os.urandom(16)).decode()
    code = hashlib.md5(uniqid(random.randint(0, sys.maxsize), True).encode('utf-8')).hexdigest()
    token = binascii.hexlify(os.urandom(32)).decode()
    checkfile = "/tmp/checkfile.txt"
    # Create signed token with Timestampsigner. to be sent to user
    signer = TimestampSigner()
    signed_token = signer.sign(token)
    data = {
        'code'  : code,
        'token' : token,
        'sToken': signed_token,
        'when'  : datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        'user'  : username,
        'filename' : binascii.hexlify(os.urandom(8)).decode()
    }
    json_data = json.dumps(data)
    if os.path.isfile(checkfile):
        os.remove(checkfile)
    # Set 600 permission for file
    original_umask = os.umask(0o177)  # 0o777 ^ 0o600
    # write json on file
    try:
        handle = os.open(checkfile, os.O_WRONLY | os.O_CREAT, 0o600)
        with os.fdopen(handle, 'w') as fout:
            #json.dumps(data, fout)
            fout.write(json_data)
            fout.close()
    finally:
        os.umask(original_umask)
    send_recovery_email(code, signed_token, email)

def send_recovery_email(code, token, email):
    """ Sends email with password reset instructions """

    try:
        template = get_template("emails/password-recover.html")
        email_context  = {
            'code'  : code,
            'token' : token,
            'host'  : get_server_host()
        }
        content  = template.render(email_context)
        email    = EmailMultiAlternatives('Recuperar contraseña', content, 'no-reply@%s' % get_server_host(), [email])
        email.send()
        p("utils.py · notificate_email", "✔ Password recover email sent successfully", '')
    except Exception as e:
        p("utils.py · notificate_email", "✕ There's a problem notifying password recover", e)

def get_active_maildomains(ldap):
    try:
        ldap.search(
            search_base   = settings.LDAP_TREE_BASE,
            search_filter = settings.LDAP_FILTERS_MAILMAN,
            attributes    = ['vd',]
        )
        return [ entry.vd.value for entry in ldap.entries ]
    except Exception as e:
        p("utils.py · get_active_maildomains", "✕ There was a problem getting mailman domains", e)

    return {}

def connect_to_postgre():
    mailman_domains = {}
    try: 
        #dsn = "postgresql:///mailman"
        dsn="host='localhost' dbname='mailman' user='postgres' password=''"
        conn = psycopg2.connect(dsn)
        cursor = conn.cursor()
        cursor.execute("SELECT * from domain")
        rows = cursor.fetchall()
        for row in rows:
            n, mail_host,_desc= row
            mailman_domains = {'mail_host': mail_host, 'description': desc }
        #new_list = [(n,mail_host,desc) for a,b in rows]
        """
        for line in rows:
            domain_val = [x[1] for x in line ]
            desc_val = [x[2] for x in line ]
            mailman_domains['mail_host']=domain_val
            mailman_domains['mail_host']=domain_val
        """
        return mailman_domains
    except Exception as e:
        print("Uh oh, connect_to_postgre - can't connect. Invalid dbname, user or password?")
        print(e)
    return {} 

def get_mailman_domains():
    mailman_domains = []
    try:
        # Data for template testing purposes
        if settings.NO_MAILMAN:
            mailman_domains = [
                { 'mail_host': 'example.com', 'description' : 'test_description '},
                { 'mail_host': 'imy.example.com', 'description' : 'test_description '},
            ]
        else:
            try:
                dsn = "postgresql:///mailman"
                #dsn="host='' dbname='mailman' user=w' password=''"
                conn = psycopg2.connect(dsn)
                cursor = conn.cursor()
                cursor.execute("SELECT * from domain")
                domains = cursor.fetchall()
                #return rows
                if domains:
                    for domain in domains:
                        domain_dic = { 'mail_host' : domain[1], 'description' : domain[2] }
                        mailman_domains.append(domain_dic)

            except Exception as e:
                print("Uh oh, can't connect. Invalid dbname, user or password?")
                print(e)

    except Exception as e:
        p("utils.py · get_mailman_domains", "✕ There was a problem getting mailman domains", e)
    return mailman_domains

def get_input_deps_fields(request, appid):
    """ Cutsom method to get dependenvies from API, that needs a user input """
    context = {}
    release = {}
    inputdeps = []
    puppet_status = {}
    current_app = {}
    default_desc = {
        'domain' : _("Esta aplicación necesita ser instalada bajo un dominio o "
                   "subdominio propio. Inserta un dominio válido que no esté "
                   "siendo usado por ninguna otra aplicación y cuyos dns estén "
                   "ya apuntando a la IP de este servidor:%s") % get_server_ip(),
        'email'  : _("Esta aplicación necesita una cuenta de email asociada. "
                    "Inserta una dirección de correo electrónico válida:"),
        'default' : _("Inserta un texto"),
    }

    if settings.NO_API:
        release_info  = debug.RELEASE
        puppet_status = debug.STATUS
    else:
        release_info  = get_release_info(request)
        puppet_status = get_puppet_status(request)
    context['status'] = puppet_status
    context['release'] = release_info['release']
    context['maintenance'] = False
    try:
        if 'groups' in release_info:
            # Get all available services, installed or not
            all_services = release_info.get('groups')
            
            current_app = [ service['dependencies'] for service in all_services if service['id'] == appid ]
            current_app_name  = [ service['name'] for service in all_services if service['id'] == appid ]
            context['appname'] = current_app_name
            print(current_app_name)
            for f, v in enumerate(current_app):
                for stri in v:
                    # Get the Description text
                    description = re.findall(r'(?s)\.\[Description:(.*?)\]', stri)

                    # Remove the Description part from API dependencies string 
                    if description:
                        clean_str = re.sub(r'(?s)\.\[Description:(.*?)\]','', stri)
                    else:
                        clean_str = stri
                    
                    #Split  dependecies string by dots 
                    b = clean_str.split(".")
                    countb = len(b)

                    # If a string has more than one dot, it means that it is a dependeency
                    #    that needs a user input 
                    if countb > 2:
                        deps = {}
                        # Remove the Description part from API dependencies string 
                        for index, value in enumerate(b):
                            # here create an indexed set of values:

                            # [0] = id
                            # [1] = type
                            # [2] = Label
                            # [3] = .[Description:.....] 
                            deps[index] = value
                            d1 = {0:'id', 1:'type', 2:'label'}
                            dependencies = dict((d1[key], value) for (key, value) in deps.items())
                            if description:
                                dependencies['Description'] = description[0]
                            else:
                                dependencies['Description'] = default_desc[b[0]]

                        inputdeps.append(dependencies)
                context['inputdeps'] = inputdeps

    except Exception as e:
        p("ServicesAvailable view", "✕ There was a problem retrieving enabled services", e)
    #if puppet_status.get('status') == 'error' or puppet_status.get('status') == 'pending':
    if puppet_status['puppetstatus'] == 'error' or puppet_status['puppetstatus'] == 'pending':
        context['maintenance'] = True
    return context 

def clean_api_description(description):
    # Get the Description text
    string = re.findall(r'(?s)\.\[Description:(.*?)\]', description)
    if string:
        return string[0]
    else: 
        return False


def get_domain_deps_apps(request):
    """ Cutsom method to get dependenvies from API, that have a domain dependency"""
    release_info = {}
    apps_with_domain = []

    if settings.NO_API:
        release_info  = debug.RELEASE
        puppet_status = debug.STATUS
    else:
        release_info  = get_release_info(request)
    try:
        if 'groups' in release_info:
            # Get all available services, installed or not
            all_services = release_info.get('groups')
            apps_with_domain = [service['id'] for service in all_services if "dependencies" in service and any("domain" in s for s in service['dependencies'])]
    except Exception as e:
        p("ServicesAvailable view", "✕ There was a problem retrieving enabled services", e)
    return  apps_with_domain


