import time
import psutil, math

# django
from django.utils.translation import ugettext_lazy as _
from django import views
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.conf import settings
from django.views.generic.edit import FormView
from django.contrib import messages
from django.contrib.auth import logout as auth_logout

# project
#from .utils import p, get_puppet_status, get_release_info, lock_cpanel, 
from . import utils
from .forms import FqdnForm
from . import debug
from ldap3 import MODIFY_REPLACE

class Details(views.View):
    """
    System details view.
    """
    graph_width  = 240
    graph_height = 120

    def get_height(self, percent):
        """ Util function to get the height of a value from the related percent. """
        return round(self.graph_height * percent/100)

    def get(self, request):
        """Handle GET requests."""
        graph_width  = self.graph_width
        graph_height = self.graph_height
        cpu_bars_n = 20
        cpu_barwidth = round(graph_width / cpu_bars_n)
        cpu_bars = [ round(cpu_barwidth * i + 5) for i in range(cpu_bars_n) ]
        # Get system memory stats
        memory    = psutil.virtual_memory()
        ram_size  = self.get_height(memory.percent)
        # Get system CPU stats
        cpu       = psutil.cpu_percent()  # percentage of used cpu
        cpu_count = psutil.cpu_count(logical=False) # number of physical cpus
        cpu_size  = self.get_height(cpu)
        # Get disk usage stats
        disk_usage     = psutil.disk_usage('/')  # gets disk usage of the partition in which the folder is
        disk_size      = self.get_height(disk_usage.percent)

        return render(request, 'pages/system-details.html', locals())

@csrf_protect
@require_POST
def get_cpu_usage(request):
    """
    View to get the CPU usage of the host machine.
    """
    cpu = psutil.cpu_percent()
    return HttpResponse(cpu, content_type="application/json")




class Reboot(views.View):
    """
    View to reboot the system.
    """

    def get(self, request):
        """Handle GET requests."""

        return render(request, 'pages/reboot.html')

    @method_decorator(csrf_protect)
    def post(self, request):
        """Handle POST requests."""

        try:
            request.ldap.modify(
                settings.LDAP_TREE_REBOOT, {
                    'info'   : [(MODIFY_REPLACE, 'ready')],
                    'status' : [(MODIFY_REPLACE, 'locked')],
                }
            )
            utils.lock_cpanel()
        except Exception as e:
            utils.p("Reboot view", "There was a problem retrieving updating cpanel/reboot attributes", e)

        return HttpResponseRedirect( reverse('logout') )


class Update(views.View):
    """
    View to update the system.
    """

    def get(self, request):
        """Handle GET requests."""

        context = {}
        if settings.NO_API:
            puppet_status = debug.STATUS
        else:
            puppet_status = utils.get_puppet_status(request)
        context['status'] = puppet_status.get('puppetstatus')
        if context['status'] == 'ready':
            if settings.NO_API:
                release_info = debug.RELEASE
            else:
                updates = 'updates'
                release_info = utils.get_release_info(request, updates)
                # TODO: check if there is a newer release , return the release info.
                if release_info:
                    context['release']     = release_info['release']
                    context['description'] = release_info['description']
                else:
                    context['status'] = 'last-update'
        elif context['status'] == 'pending':
            context['release'] = 'pending'
        else:
            context['status'] = 'no-updates'
        return render(request, 'pages/update.html', context)

    @method_decorator(csrf_protect)
    def post(self, request):
        """Handle POST requests."""

        try:
            release = request.POST.get("release")
            request.ldap.modify(
                settings.LDAP_TREE_CPANEL, {
                    'type'   : [(MODIFY_REPLACE, release)],
                    'status' : [(MODIFY_REPLACE, 'locked')],
                }
            )

        except Exception as e:
            utils.p("Update view", "There was a problem updating cpanel attributes", e)

        return HttpResponseRedirect( reverse('logout') )

class Fqdn(FormView):
    """ 
    This Form allows to change the FQDN of the server 
    The hostname must be preserverd, but the assoicated domain can be changed.
    The process needs following actions and checks:
    - check that domain is not in ldap or in use by other applications
    - check dns entries for new desired fqdn
    - save the old fqdn value in old_domain attribute in ldap (used by puppet)
    - activate all deactivate groups (if there are any)
    - Lock cpanel and destroy session
    """

    template_name = 'pages/fqdn.html'
    form_class    = FqdnForm
    domains       = {}

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context               = super(Fqdn, self).get_context_data(**kwargs)
        context['fqdn'] = self.fqdn
        context['servername'] = self.servername
        # get the server domain : remove servername from fqdn
        # didn't find a python function equivalent to hostname -d 
        context['server_domain'] = self.server_domain
        context['display_form'] = 'form' in kwargs
        return context

    def get_form(self):
        """Return an instance of the form to be used in this view."""
        self.fqdn = utils.get_server_host()
        self.servername = utils.get_server_hostname()
        replacing_string = "%s." % self.servername
        self.server_domain = self.fqdn.replace(replacing_string, "", 1)
        domains = utils.get_existing_domains(self.request.ldap)
        domain_list = [ domain.vd.value for domain in domains ]
        domains_in_use = utils.domain_is_in_use(self.request.ldap)
        domains_used_list = [ domain.status.value for domain in domains_in_use]
        return FqdnForm( domain_list=domain_list, domains_used_list=domains_used_list,servername=self.servername, **self.get_form_kwargs())

    def form_valid(self, form):
        newdomain = form['name'].value()
        custom_logs = 'true' if form['log_server'].value() == True else 'false'
        #TODO: if we use .lower in bool_kdap() we can just use
        #custom_logs = form['log_server'].value()
        base_dn = "ou=conf,%s" %  settings.LDAP_TREE_CPANEL
        errors=False
        """If the form is valid, update values in ldap """

        try:
            dn = "ou=fqdn_domain,%s" % base_dn 
            self.request.ldap.modify(dn, {
                'status' : [(MODIFY_REPLACE, newdomain)]
            })
            # update old_domain value
            # normally  object ou=fqdn_domain_old does not exixts, create it 
            # else update status value
            dn = "ou=fqdn_domain_old,%s" % base_dn
            try:
                self.request.ldap.add(dn, ['organizationalUnit', 'metaInfo', 'top'], {
                    'status'            : self.server_domain,
                })
            except Exception as e:
                if e.result == 68: 
                    print('domain old ya existe')
                    self.request.ldap.modify(dn, {
                    'status' : [(MODIFY_REPLACE, self.server_domain)]
                })

            # Update custom logs settings
            dn = 'ou=logmail_custom,%s' % base_dn
            self.request.ldap.modify(dn, {
                'status' : [(MODIFY_REPLACE, custom_logs )]
            })
            # Activate all deactivated groups
          
            self.request.ldap.search(
                settings.LDAP_TREE_BASE,
                settings.LDAP_FILTERS_INSTALLED_DISABLED_SERVICES,
                attributes=['ou']
            )
            disabled_service_names = [ service.ou.value for service in self.request.ldap.entries ]
            print(disabled_service_names)
            if (disabled_service_names):
                for service in disabled_service_names:
                    dn = 'ou=%s,%s' % (service, settings.LDAP_TREE_SERVICES)
                    self.request.ldap.modify(dn, {
                        'type'   : [(MODIFY_REPLACE, 'available')],
                        'status' : [(MODIFY_REPLACE, 'enabled')]
                    })

        except RuntimeError as re:
            print("There was a problem updating ldap data")
            print(re)
            errors=True

        except Exception as e:
            messages.error(self.request, _('Ha habido un error y no se puede continuar con la operación. '
                                           'Si el problema persiste contacta '
                                           'con los administrador-s'))

            print("There was a problem updating")
            print(e)
            errors=True
        #if everything is ok, lock cpanel
        # Avoid unlog user if something went wrong
        if not errors:
            # set ou=customfqdn,ou=cpanel, status as locked
            dn = "ou=customfqdn,%s" %  settings.LDAP_TREE_CPANEL 
            self.request.ldap.modify(dn, {
                'status' : [(MODIFY_REPLACE, 'locked')]
            })

            # time.sleep(t) because local cpanel must run befor puppet master
            time.sleep(1)
            # lock cpanel ad log out user
            utils.lock_cpanel(self.request) 
            # Manually destroy session
            username = self.request.session['user']
            del self.request.session['user']
            del self.request.session['session_key']
            # Destroy cookies
            next_page = 'changesystem'
            response  = HttpResponseRedirect(reverse(next_page), locals)
            response.delete_cookie('s')
            response.set_cookie('oldDomain', self.servername + '.' + self.server_domain)
            response.set_cookie('newDomain', self.servername + '.' + newdomain)
            utils.p("Logout view", "this is username: ", username)
            # redirect to settings.LOGOUT_REDIRECT_URL
            auth_logout(self.request)
            return response
        else: 
            return super(Fqdn, self).form_valid(form)
        
class NewFqdnSet(views.View):
    def get(self, request):
        # TODO: if user is logged in redirect to cpanel homepage (details)
        return render(request, 'registration/fqdn-change.html', locals())
