# django
from django import views
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect
from django.contrib import messages
# project
from . import utils
from django.conf import settings

class DeleteEntry(views.View):
    """ View to delete an entry from LDAP """

    def post(self, request):
        try:
            dn  = request.POST.get('dn')
            url = request.POST.get('url')
            # Delete item recursively
            utils.recursive_delete(self.request.ldap, dn)
            messages.success(self.request, _('Elemento eliminado con éxito'))
            return HttpResponseRedirect(url)
        except Exception as e:
            if settings.DEBUG:
                print(e)
            messages.error(self.request, _('Hubo algún problema eliminando la cuenta de correo. '
                                           'Contacta con los administrador-s si el problema persiste'))
            return HttpResponseRedirect(url)
