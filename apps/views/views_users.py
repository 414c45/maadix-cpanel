# python
import grp, math, json
from datetime import datetime
# django
from django.utils.translation import ugettext_lazy as _
from django import views
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from django.urls import reverse_lazy, reverse
from django.contrib import messages
from django.utils.html import escape
from django.contrib.auth.views import LogoutView as logout
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.contrib.auth import logout as auth_logout
from django.core.signing import TimestampSigner
# contrib
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA, MODIFY_ADD, MODIFY_DELETE
from ldap3.utils.hashed import hashed
from ldap3.core.exceptions import LDAPSocketOpenError
# project
from django.conf import settings
from . import utils, forms


class LoginView(FormView):
    """ Login page """

    form_class    = forms.CustomLoginForm
    template_name = 'registration/login.html'

    def get_success_url(self, username):
        """ Redirection url if form is valid. Depends on user's role """
        role = utils.get_user_role(username)
        if role == 'postmaster':
            return reverse('mails')
        if role == 'email':
            return reverse('email') + "?mail=" + username
        return reverse('system-details')

    def form_valid(self, form):
        password = form["password"].value()
        username = form["username"].value()
        bad_credentials = _("Las credenciales son incorrectas.")
        ldap = utils.connect_ldap(username, password)
         # Logi = utils.connect_ldap(username, password)
        if ldap['connection']:
            # Create response, set success_url depending on users role
            response = HttpResponseRedirect( self.get_success_url(username))
            # Encrypt password with one time pad
            enc_pass, key = utils.enc_pwd(password)
            #self.request.session['user_status'] = check_activate
            """ Use django file-based session 
                storing key and username in session and encrypted password in cookie
            """
            self.request.session['session_key'] = key
            self.request.session['user'] = username.strip()
            response.set_cookie('s', enc_pass)
            return response
        elif ldap['error'] == 'LDAPSocketOpenError':
            # If socket error, ldap is shut down for some reasons
            # warn the user accordingly
            messages.error(self.request, _('Hay problemas de conectividad con la base de datos.'
                                      'Intenta entrar dentro de unos minutos. Disculpa las '
                                      'molestias. Si el problema persiste contacta con nosotras'))
            return HttpResponseRedirect( reverse('login') )
        elif ldap['error'] == 'LDAPInvalidCredentialsResult':
            messages.error(self.request, _('Las credenciales son incorrectas. ¿Las has escrito correctamente?'))
            return HttpResponseRedirect( reverse('login') )

    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        if (request.COOKIES.get('s')):
            username = self.request.session['user']
            role = utils.get_user_role(username)
            if role == 'postmaster':
                home_page = reverse('mails')
            if role == 'email':
                home_page = reverse('email') + "?mail=" + username 
            else:
                home_page = reverse('system-details')
            response  = HttpResponseRedirect(home_page)
            return response
        
        elif request.method.lower() in self.http_method_names:
            handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        else:
            handler = self.http_method_not_allowed
        return handler(request, *args, **kwargs)

class LogoutView(logout):
    """ Custom logout view that destroys session data """

    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        next_page = self.get_next_page()
        # clean session data
        response  = HttpResponseRedirect(next_page)
        username = request.session['user']
        del request.session['user']
        del request.session['session_key']
        # Destroy cookies
        response.delete_cookie('s')
        utils.p("Logout view", "this is username: ", username)
        # redirect to settings.LOGOUT_REDIRECT_URL
        messages.success(request, _('Hasta la próxima!'))
        auth_logout(request)
        return response


class PasswordRecovery(FormView):
    """ Login page """

    form_class    = forms.PasswordRecoveryForm
    template_name = 'registration/password-recovery.html'

    def get_success_url(self):
        return reverse('login')

    def get_form(self):
        """Return an instance of the form to be used in this view."""

        # password recovery is a login exempted view, so we're not connected to ldap yet
        ldap  = utils.anonymous_connect_ldap()
        admin = utils.get_admin(ldap)
        return forms.PasswordRecoveryForm(
            admin_name=admin.cn.value,
            admin_email=admin.email.value,
            **self.get_form_kwargs()
        )

    def form_valid(self, form):
        """Security check complete. Log the user in."""

        utils.password_recover( form['username'].value(), form['email'].value() )
        messages.success(self.request, _('Te hemos enviado instrucciones para '
                                         'cambiar tu contraseña al correo electrónico '
                                         'que has introducido'))

        return super(PasswordRecovery, self).form_valid(form)


class PasswordReset(FormView):
    """ Password reset view """

    form_class    = forms.PasswordResetForm
    template_name = 'registration/password-reset.html'

    def get_success_url(request):
        return reverse('login')

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context  = super(PasswordReset, self).get_context_data(**kwargs)
        context['error'] = self.error
        return context

    def get_form(self):
        """ Return an instance of the form to be used in this view."""
        # Get token from url
        self.token = self.request.GET.get('token', None)
        unsigned_token = ''
        self.error = True
        with open("/tmp/checkfile.txt", "r") as checkfileisigned:
            signer = TimestampSigner()
            self.data = json.load(checkfileisigned)
            #signed_token = self.data.get('sToken')
            # Get the token witout signature stored in checkfile to compare with unsigned token
            plain_token = self.data.get('token')
            self.filename = self.data.get('filename')
            try:
                unsigned_token = signer.unsign(self.token, max_age=36000)
                # If token matches show form
            except Exception as e:
                self.error = True
                

            if unsigned_token == plain_token:
                self.error = False
                return forms.PasswordResetForm(
                    code=self.data.get('code'),
                    user=self.data.get('user'),
                    **self.get_form_kwargs()
                )
            # If not, this is not a valid token
            if(self.error):
                messages.error(self.request, _("La URL no es válida o ha caducado"))



    def form_valid(self, form):
        """ Actions to be performed if the form is valid. """

        password      = hashed(HASHED_SALTED_SHA, form['password'].value())
        ldif_filename = "/tmp/update-%s.ldif" % self.filename
        dn            = "cn=%s,%s" % ( form['username'].value(), settings.LDAP_TREE_BASE )
        with open(ldif_filename, "w") as ldif_file:
            ldif_file.write('\n'.join([
                "dn: %s" % dn,
                "changetype: modify",
                "replace: userpassword",
                "userpassword: %s" % password])
            )
        messages.success(self.request, _("Procesando operación. Todos los datos para recuperar "
                                    "la contraseña han sido insertados correctamente. Recibirás un correo "
                                    "de confirmación en cuanto la contraseña haya sido restablecida."
                                    "Este proceso puede tardar hasta 10 minutos."))
        return super(PasswordReset, self).form_valid(form)


class Profile(FormView):
    """ User profile view """

    form_class    = forms.ProfileForm
    template_name = 'pages/profile.html'

    def get_success_url(self, pwd):
        """Returns the URL to redirect user after a succesful submit"""
        # logout if password was updated
        if pwd:
            return reverse('logout')
        return self.request.get_full_path()

    def get_form(self):
        """ Return an instance of the form to be used in this view."""

        username = self.request.user.username
        self.role     = utils.get_user_role(username)
        if self.role == 'admin':
            userfilter = "(&(objectClass=extensibleObject)(cn=%s))" % username
        else:
            userfilter = "(mail=%s)" % username
        self.request.ldap.search(
            settings.LDAP_TREE_BASE,
            userfilter,
            attributes=['email']
        )
        user  = self.request.ldap.entries[0]
        kwargs = self.get_form_kwargs()
        kwargs['initial'] = {
            'username' : username,
            'email'    : user.email.value,
        }
        return forms.ProfileForm(
            pwd  = utils.dec_pwd(self.request),
            role = self.role,
            **kwargs
        )

    def form_valid(self, form):
        """ Actions to be performed if the form is valid. """

        username = self.request.user.username
        pwd      = ''
        # set fields to be updated
        fields = {}
        if 'email' in form.fields and form['email'].value():
            fields['email'] = [(MODIFY_REPLACE, form['email'].value())]
        if 'password' in form.fields and form['password'].value():
            pwd = form['password'].value()
            fields['userpassword'] = [(MODIFY_REPLACE, hashed(HASHED_SALTED_SHA, pwd))]
        # build dn of the current user
        if self.role == 'admin':
            dn     = "cn=%s,%s" % (username, settings.LDAP_TREE_BASE)
        elif self.role == 'postmaster':
            domain = username.split('@')[1]
            dn     = "cn=postmaster,vd=%s,%s" % (domain, settings.LDAP_TREE_HOSTING)
        else:
            domain = username.split('@')[1]
            dn     = "mail=%s,vd=%s,%s" % (username, domain, settings.LDAP_TREE_HOSTING)
        # modify user fields
        self.request.ldap.modify(dn, fields)
        if pwd:
            messages.success(self.request, _('Has cambiado tu perfil con éxito. '
                                             'Utiliza la nueva contraseña para entrar.'))
        else:
            messages.success(self.request, _('Has cambiado tu perfil con éxito.'))
        return HttpResponseRedirect(self.get_success_url(pwd))

class Users(FormView):
    """ Email accounts list view and form to add a new email account"""

    template_name = 'pages/users.html'
    form_class    = forms.UserForm
    ldap          = {}
    emails        = []

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_form(self):
        """Return an instance of the form to be used in this view."""

        self.emails = utils.get_existing_emails(self.request.ldap)
        return forms.UserForm(
            emails=self.emails,
            services=self.request.enabled_services,
            edit_form=False,
            #ldap=self.request.ldap,
            **self.get_form_kwargs()
        )

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        users = []
        try:
            # Get users and enabled services for each one
            self.request.ldap.search(
                search_base   = settings.LDAP_TREE_USERS,
                search_filter = settings.LDAP_FILTERS_SFTP,
                attributes    = ['uid','authorizedservice','userpassword']
            )
            for user in self.request.ldap.entries:
                authorized_services = user.authorizedservice.value
                users.append({
                    'username'  : user.uid.value,
                    'dn'        : user.entry_dn,
                    'sftp'      : utils.check_value('sshd',    authorized_services),
                    'apache'    : utils.check_value('apache',  authorized_services),
                    'openvpn'   : utils.check_value('openvpn', authorized_services),
                })
            users = sorted(users, key = lambda i: i.get('username'))
            # Check if users are webmasters and domains maintained by them
            # We iterate twice because ldap entries are overwritten in every call to search()
            for user in users:
                try:
                    self.request.ldap.search(
                        search_base   = settings.LDAP_TREE_BASE,
                        search_filter = "(&(vd=*)(adminid=%s))" % user['username'],
                        attributes    = ['vd']
                    )
                    if self.request.ldap.entries:
                        user['domains'] = [ domain.vd for domain in self.request.ldap.entries ]
                except Exception as e:
                    utils.p("✕ view_users.py", "There's a problem fetching users from LDAP: ", e)
        except Exception as e:
            utils.p("✕ view_users.py", "There's a problem fetching users from LDAP: ", e)
        enabled_services=self.request.enabled_services
        context = super(Users, self).get_context_data(**kwargs)
        context['users']        = users
        context['phpmyadmin']   = True if 'phpmyadmin' in enabled_services else False
        context['vpn']          = True if 'openvpn' in enabled_services else False
        context['display_form'] = 'form' in kwargs
        return context

    def form_valid(self, form):
        """If the form is valid, save the associated model."""

        user           = form['username'].value()
        name           = form['name'].value()
        surname        = form['surname'].value()
        email          = form['email'].value()
        uidnumber      = None
        password       = form['password'].value()
        services       = []

        for service in ['openvpn', 'apache']:
            if service in form.fields and form[service].value():
                services.append(service)
        self.request.ldap.search(
            search_base   = settings.LDAP_TREE_BASE,
            search_filter = "(&(objectClass=uidNext)(uidnumber=*))",
            attributes    = ['uidnumber']
        )
        next_uid = int(self.request.ldap.entries[0].uidnumber.value) + 1
        if next_uid:
            uidnumber = next_uid
            try:
                dn = 'cn=uidNext,ou=sshd,ou=People,dc=example,dc=tld'
                self.request.ldap.modify(dn, { 'uidnumber' : [(MODIFY_REPLACE, next_uid)] })
            except Exception as e:
                utils.p("✕ view_users.py", "There was a problem updating next uid value: ", e)

        if form['sshd'].value():
            services.append('sshd')
            home_directory = "/home/sftpusers/%s" % user
            login_shell    = "/bin/bash"
            gidnumber     = grp.getgrnam('sftpusers')[2]
        else:
            home_directory = "none"
            login_shell    = "none"
            gidnumber      = uidnumber

        dn = 'uid=%s,%s' % (user, settings.LDAP_TREE_USERS)
        attributes = {
                'uid'               : user,
                'uidnumber'         : uidnumber,
                'gidnumber'         : gidnumber,
                'gecos'             : '%s,,,' % user,
                'cn'                : name if name else user,
                'sn'                : surname if surname else user,
                'mail'              : email,
                'loginshell'        : login_shell,
                'homedirectory'     : home_directory,
                'shadowlastchange'  : math.floor(utils.unix_timestamp() / 86400),
                'shadowmax'         : '99999',
                'shadowwarning'     : '7',
                'userpassword'      : hashed(HASHED_SALTED_SHA, password),
            }
        if services:
            attributes['authorizedservice'] = services
        try:
            self.request.ldap.add(dn, [
                'person',
                'organizationalPerson',
                'inetOrgPerson',
                'posixAccount',
                'top',
                'shadowAccount',
                'authorizedServiceObject'
            ], attributes 
            )
            if form['instructions'].value():
                utils.send_vpn_instructions(self.request, email, user)
            ### Add the user to the web group, if ssh is enabes
            if form['sshd'].value():
                try:
                    dn = 'cn=web,ou=groups,ou=People,%s' % (settings.LDAP_TREE_BASE)
                    self.request.ldap.modify(dn, {
                        'memberuid'         : [(MODIFY_ADD, user)],
                    })
                except Exception as e:
                    utils.p("✕ view_users.py", "There was a problem creating the user %s :" % user, e)

            messages.success(self.request, _('Usuari- añadid- con éxito'))
            utils.p("✔ view_users.py", "Usuari- %s añadid- con éxito':" % user, '')
        except Exception as e:
            utils.p("✕ view_users.py", "There was a problem creating the user %s :" % user, e)
        
        return super(Users, self).form_valid(form)

class User(FormView):
    """ Form to edit a user account"""

    template_name = 'pages/user.html'
    form_class    = forms.UserForm

    def get_success_url(self):
        return reverse('users')

    def get_form(self):
        """Return an instance of the form to be used in this view."""

        username = self.request.GET.get('username')
        emails = utils.get_existing_emails(self.request.ldap)
        self.emails  = [ email.mail for email in emails ]
        self.domains = utils.get_user_domains(self.request.ldap, username)
        try:
            self.request.ldap.search(
                search_base   = settings.LDAP_TREE_BASE,
                search_filter = "(uid=%s)" % username,
                attributes = [
                    'uid',
                    'uidnumber',
                    'gidnumber',
                    'gecos',
                    'cn',
                    'sn',
                    'mail',
                    'gidnumber',
                    'loginshell',
                    'homedirectory',
                    'shadowlastchange',
                    'shadowmax',
                    'shadowwarning',
                    'authorizedservice',
                ]
            )
            self.user = self.request.ldap.entries[0]
            kwargs    = self.get_form_kwargs()
            services  = self.user.authorizedservice
            kwargs['initial'] = {
                'username' : self.user.uid.value,
                'name'     : self.user.cn.value,
                'surname'  : self.user.sn.value,
                'email'    : self.user.mail.value,
                'home_dir' : self.user.homedirectory.value,
                'sshd'     : 'sshd' in services,
                'openvpn'      : 'openvpn' in services,
                'apache'   : 'apache' in services
            }
            return forms.UserForm(
                emails=self.emails,
                domains=self.domains,
                services=self.request.enabled_services,
                edit_form=True,
                **kwargs
            )
        except Exception as e:
            print("There was a problem retrieving the user: ")
            print(str(e))

        return forms.UserForm(
            emails=self.emails,
            domains=self.domains,
            services=self.request.enabled_services,
            edit_form=True,
            **self.get_form_kwargs()
        )

    def form_valid(self, form):
        """If the form is valid, save the associated model."""

        user           = form['username'].value()
        name           = form['name'].value()
        surname        = form['surname'].value()
        email          = form['email'].value()
        password       = form['password'].value()
        services       = []
        for service in ['openvpn', 'apache', 'sshd']:
            if service in form.fields and form[service].value():
                services.append(service)
        dn = 'uid=%s,%s' % (user, settings.LDAP_TREE_USERS)
        # Update user group if sshd is activated. It must be the sftpusers guidnumber
        attr = {
                'cn'                : [(MODIFY_REPLACE, name if name else user)],
                'sn'                : [(MODIFY_REPLACE, surname if surname else user)],
                'mail'              : [(MODIFY_REPLACE, email)],
                'shadowlastchange'  : [(MODIFY_REPLACE, math.floor(utils.unix_timestamp() / 86400))],
                'authorizedservice' : [(MODIFY_REPLACE, services)],
            }
        # Only update password if it has been changed
        if password:
            attr['userpassword'] = [(MODIFY_REPLACE, hashed(HASHED_SALTED_SHA, password))]
        # Update user group if sshd is activated. It must be the sftpusers guidnumber
        if 'sshd' in services:
            home_directory = "/home/sftpusers/%s" % user
            login_shell    = "/bin/bash"
            gidnumber     = grp.getgrnam('sftpusers')[2]
            attr['homedirectory'] = [(MODIFY_REPLACE, home_directory)]
            attr['loginshell'] = [(MODIFY_REPLACE, login_shell)]
            attr['gidnumber'] = [(MODIFY_REPLACE, gidnumber)]

        try:
            self.request.ldap.modify(dn, attr)
            if form['instructions'].value():
                utils.send_vpn_instructions(self.request, email, user)
            # Update web group depending on sshd service setting
            # cn=web,ou=groups,ou=People 
            # First check if user exists in web group
            dn = "cn=web,ou=groups,ou=People,%s" % settings.LDAP_TREE_BASE
            try:
                self.request.ldap.search(
                    search_base   = dn, 
                    search_filter = "(&(memberuid=%s))" % user,
                    attributes    = ['memberuid']
                )
                web_user = self.request.ldap.entries
            
            except Exception as e:
                utils.p("✕ view_users.py", "There was a problem updating the user %s :" % user, e)
            # if user has not sshd and exists in web group, remove from froup
            if not 'sshd' in services and not user in web_user:
                attr = {'memberuid' : [(MODIFY_DELETE, user)]}
            # if user has sshd but is not in web group, add to the group
            elif 'sshd' in services and not web_user:
                attr = {'memberuid' : [(MODIFY_ADD, user)]}
            try:
                self.request.ldap.modify(dn, attr)    
            except Exception as e:
                utils.p("✕ view_users.py", "There was a problem updating the web group for  user %s :" % user, e)

            messages.success(self.request, _('Usuario modificado con éxito'))
            utils.p("✔ view_users.py", "Usuari- %s modificdo - con éxito':" % user, '')
        except Exception as e:
            messages.error(self.request, _('Ha habido un error modificando al usuario. '
                                           'Los cambios no se han guardado. Disculpa las '
                                           'molestias. Si el problema persiste contacta '
                                           'con los administrador-s'))
            utils.p("✕ view_users.py", "There was a problem updating the user %s :" % user, e)

        return super(User, self).form_valid(form)


    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context  = super(User, self).get_context_data(**kwargs)
        username = self.request.GET.get('username')
        context['username'] = username

        return context

class SuperUser(FormView):
    """
    Form to edit the superuser account.
    """

    template_name = 'pages/superuser.html'
    form_class    = forms.SuperuserForm

    def get_success_url(self):
        return self.request.get_full_path()

    def get_form(self):
        """Return an instance of the form to be used in this view."""

        # get existing emails
        emails = utils.get_existing_emails(self.request.ldap)
        self.emails = [ email.mail for email in emails ]

        try:
            self.request.ldap.search(
                search_base   = settings.LDAP_TREE_BASE,
                search_filter = settings.LDAP_FILTERS_SUPERUSER,
                attributes = [
                    'uid',
                    'cn',
                    'sn',
                    'mail',
                    'homedirectory',
                    'authorizedservice',
                ]
            )

            self.user = self.request.ldap.entries[0]
            kwargs    = self.get_form_kwargs()
            services  = self.user.authorizedservice
            kwargs['initial'] = {
                'username' : self.user.uid.value,
                'name'     : self.user.cn.value,
                'surname'  : self.user.sn.value,
                'email'    : self.user.mail.value,
                'openvpn'  : 'openvpn' in services,
                'apache'   : 'apache' in services,
                'home_dir' : self.user.homedirectory.value,
            }
            return forms.SuperuserForm(
                emails   = self.emails,
                services = self.request.enabled_services,
                password = utils.dec_pwd(self.request),
                **kwargs
            )
        except Exception as e:
            print("There was a problem retrieving the user: ")
            print(str(e))

        return forms.SuperuserForm(
            emails   = self.emails,
            services = self.request.enabled_services,
            **self.get_form_kwargs()
        )

    def form_valid(self, form):
        """If the form is valid, save the associated model."""

        user           = self.user
        username       = self.user.uid.value
        name           = form['name'].value()
        surname        = form['surname'].value()
        email          = form['email'].value()
        password       = form['password'].value()
        # Always give superuser cron and sshd autorization
        services       = ['cron', 'sshd']
        for service in ['openvpn', 'apache']:
            if service in form.fields and form[service].value():
                services.append(service)
        dn = 'uid=%s,%s' % (username, settings.LDAP_TREE_USERS)
        attr = {
                'cn'                : [(MODIFY_REPLACE, name if name else username)],
                'sn'                : [(MODIFY_REPLACE, surname if surname else '')],
                'mail'              : [(MODIFY_REPLACE, email if email else '')],
                'shadowlastchange'  : [(MODIFY_REPLACE, math.floor(utils.unix_timestamp() / 86400))],
                'authorizedservice' : [(MODIFY_REPLACE, services)],
             #   'userpassword'      : [(MODIFY_REPLACE, hashed(HASHED_SALTED_SHA, password))],
            }
        if password:
            attr['userpassword'] = [(MODIFY_REPLACE, hashed(HASHED_SALTED_SHA, password))]
        try:
            self.request.ldap.modify(dn, attr)
            if form['instructions'].value():
                utils.send_vpn_instructions(self.request, email, username)
            messages.success(self.request, _('Supersuario modificado con éxito'))
        except Exception as e:
            messages.error(self.request, _('Ha habido un error modificando al superusuario. '
                                           'Los cambios no se han guardado. Disculpa las '
                                           'molestias. Si el problema persiste contacta '
                                           'con los administrador-s'))
            utils.p("✕ view_users.py", "There was a problem updating superuser %s :" % user, e)
        return super(SuperUser, self).form_valid(form)

class Postmasters(views.View):
    """
    Postmasters list view.
    """
    def get(self, request):
        active_postmasters = []
        # get domains in ldap 
        self.request.ldap.search(
            search_base   = settings.LDAP_TREE_BASE,
            search_filter = '(vd=*)',
            attributes    = [ 'vd' ],
        )
        postmasters = { entry.vd.value : 0 for entry in self.request.ldap.entries }
        # Create list with domains, to look for created postmasters
        existing_domains = [ entry.vd.value  for entry in self.request.ldap.entries ] 
        for domain in existing_domains:
            try:
                self.request.ldap.search(
                    search_base   = 'cn=postmaster,vd=%s,%s' % (domain, settings.LDAP_TREE_HOSTING),
                    search_filter = '(objectClass=VirtualMailAlias)',
                    attributes    = [ 'cn' ]
                )
                postmaster = self.request.ldap.entries
            except:
                postmaster = False
            if postmaster:
                active_postmasters.append(domain)

        # get emails
        self.request.ldap.search(
            search_base   = 'o=hosting,dc=example,dc=tld',
            search_filter = '(objectClass=VirtualMailAccount)',
            attributes    = ['mail', ]
        )
        # update number of emails related to each postmaster
        for entry in self.request.ldap.entries:
            domain = entry.mail.value.split('@')[1]
            if domain in postmasters:
                postmasters[domain] += 1

        return render(request, 'pages/postmasters.html', {
            'postmasters' : postmasters, 'active_postmasters' : active_postmasters 
        })

class Postmaster(FormView):
    """
    Form to edit users with the role postmaster.
    """

    template_name = 'pages/postmaster.html'
    form_class    = forms.PostmasterForm

    def get_success_url(self):
        return reverse('postmasters')

    def get_form(self):
        """Return an instance of the form to be used in this view."""

        self.domain = self.request.GET.get('domain')
        kwargs = self.get_form_kwargs()
        kwargs['initial']['username'] = 'postmaster@%s' % self.domain
        return forms.PostmasterForm(**kwargs)

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""

        context  = super(Postmaster, self).get_context_data(**kwargs)
        #domain = self.request.GET.get('domain')
        context['domain'] = self.domain
        return context

    def form_valid(self, form):
        """If the form is valid, save the associated model."""

        password = hashed(HASHED_SALTED_SHA, form['password'].value())

        dn = 'cn=postmaster,vd=%s,%s' % (self.domain, settings.LDAP_TREE_HOSTING)
        # Check if postmaster exists fro current domain
        try:
            self.request.ldap.search(
                search_base   = dn,
                search_filter = '(objectClass=VirtualMailAlias)',
                attributes    = [ 'cn' ]
            )
            postmaster = self.request.ldap.entries
        except:
            postmaster = False
        # If postmaster exists, edit it, else create entry 
        if postmaster: 
            try:
                self.request.ldap.modify(dn, {
                    'userpassword'      : [(MODIFY_REPLACE, password)],
                })
                messages.success(self.request, _('Contraseña modificada con éxito'))
            except Exception as e:
                messages.error(self.request, _('Ha habido un error modificando el postmaster. '
                                               'Los cambios no se han guardado. Disculpa las '
                                               'molestias. Si el problema persiste contacta '
                                               'con los administrador-s'))
                utils.p("✕ view_users.py", "There was a problem updating webmaster" , e)
        else:
            try:
                self.request.ldap.add(dn, ['VirtualMailAlias', 'top'], {
                    'cn'            : 'Postmaster',
                    'sn'            : 'Postmaster',
                    'mail'          : 'postmaster@%s' % self.domain,
                    'maildrop'      : 'postmaster',
                    'accountActive' : 'TRUE',
                    'creationDate'  : utils.ldap_creation_date(),
                    'lastChange'    : utils.unix_timestamp(),
                    'userpassword'  : password
                })
                messages.success(self.request, _('Cuenta de postmaster activada con éxito'))
            except Exception as e:
                messages.error(self.request, _('Ha habido un error al activando el postmaster. '))
                utils.p("✕ view_users.py", "There was a problem creating webmaster" , e) 

        return super(Postmaster, self).form_valid(form)
