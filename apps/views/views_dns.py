# python
import os, re
# django
from django import views
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from django.urls import reverse_lazy, reverse
from django.utils.encoding import force_bytes
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
# project
from . import utils
from .forms import AddDomainForm, EditDomainForm
from django.conf import settings


class DnsView(views.View):
    """
    Domains list view.
    """

    MESSAGES = {
        'NOT_A_RECORD'   : _("No se ha encontrado ningún registro para el dominio."),
        'A_RECORDS'      : _("El dominio tiene configurado más de un registro de tipo A. "
                             "Esta configuración puede provocar anomalías. A menos que sepas "
                             "exactamente lo que estás haciendo es aconsejable que dejes un "
                             "solo registro."),
        'OK'             : _("La configuración de DNS para el servidor web es correcta"),
        'NOT_OK'         : _("La configuración de los DNS no es correcta para el servidor web. "
                             "Sigue las instrucciones a continuación para corregirla"),
        'NOT_MAILSERVER' : _("El servidor de correo no está activado para este dominio. "
                             "En el caso quisieras activarlo la siguiente tabla te muestra los valores "
                             "DNS correctos."),
        #'CHANGE_IP'      : _("Edita el registro de tipo A cambiando la actual IP %s por %s"),
    }

    def get(self, request):
        # Domain general info
        ip             = utils.get_server_ip()
        host           = utils.get_server_host()
        domain         = request.GET.get('domain')
        edit_url       = '%s?domain=%s' % (reverse('domains-edit'), domain)

        # Domain DNS related info
        domain_records = utils.get_dns_records(domain)
        has_A          = 'A' in domain_records
        record         = domain_records['A'][0] if has_A else None
        records_A      = domain_records['A'] if has_A else [None]
        has_MX         = 'MX' in domain_records
        valid_MX       = host +'.'
        records_MX     = [ mx.to_text().split()[1] for mx in domain_records['MX'] ] if has_MX else [None]
        has_TXT        = 'TXT' in domain_records
        record_SPF    = [ txt for txt in domain_records['TXT'] if 'v=spf' in txt.to_text() ][0] if has_TXT else [None]
        valid_SPF      = "\"v=spf1 mx ip4:%s a:%s ~all\"" % ( ip, host)
        record_DKIM   = utils.get_dkim(domain)
        has_dkim       = None
        dkim_domain    = "default._domainkey.%s" % domain
        msg            = {}

        try:
            # look for the domain in LDAP
            request.ldap.search(
                settings.LDAP_TREE_BASE,
                '(vd=%s)'%domain,
                attributes=['accountActive']
            )
            # if querying a wrong domain redirect to domains overview
            if len(request.ldap.entries) == 0:
                messages.error(self.request, _('No tienes ese dominio registrado, ¿lo has escrito correctamente?'))
                return HttpResponseRedirect( reverse_lazy('domains') )

            # Check A records
            if not has_A:
                msg['A'] = self.MESSAGES['NOT_A_RECORD']
            else:
                A_records_number = len(domain_records['A'])
                if  A_records_number == 1 and '%s' % record == '%s' % ip:
                    msg['A'] = self.MESSAGES['OK']
                elif A_records_number > 1 and ip in records_A:
                    msg['A'] = self.MESSAGES['A_RECORDS']
                else:
                    msg['A'] = self.MESSAGES['NOT_OK']

            # Check MX records
            if has_MX:
                MX_records = [ mx.to_text()[:-1] for mx in domain_records['MX'] ]
                #valid_MX   = host 

            mail_active = utils.ldap_val(request.ldap.entries[0].accountActive.value)
            if not mail_active:
                msg['MX'] = self.MESSAGES['NOT_MAILSERVER']

            # Check DKIM record
            try:
                request.ldap.search(
                    "ou=%s,ou=opendkim,ou=cpanel,%s" % ( domain, settings.LDAP_TREE_BASE),
                    "(objectClass=organizationalUnit)",
                )
            except Exception as e:
                print("There was a problem retrieving DKIM entry in LDAP: ")
                print( str(e) )

            has_dkim = len(request.ldap.entries) > 0
            if has_dkim or not mail_active:
                dkim_path = "/etc/opendkim/keys/%s/default.txt" % domain
                if os.path.isfile(dkim_path):
                    dkim_file = open(dkim_path).read(1000)
                    key = re.search('"p=(.*)"', dkim_file)
                    msg['DKIM'] = "v=DKIM1; k=rsa; %s" % key.group(0)[1:-1]
                else:
                    msg['DKIM'] = _('Todavía no se ha generado ninguna clave dkim para el dominio %s. Este proceso puede tardar unos minutos.' % domain)
            else:
                msg['DKIM'] = _('La clave DKIM no está activada para este dominio')
        except Exception as e:
            if settings.DEBUG:
                print("There was a problem retrieving the domain conf: ")
                print(e)

        return render(request, 'pages/dns.html', locals())
