# python
import os
# django
from django import views
from django.views.generic.edit import FormView
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.contrib import messages
from django.utils.encoding import force_bytes
from django.utils.translation import ugettext_lazy as _
# contrib
from ldap3 import HASHED_SALTED_SHA
from ldap3.utils.hashed import hashed
from ldap3 import MODIFY_REPLACE
# project
from . import utils
from .forms import AddDomainForm, EditDomainForm
from django.conf import settings


class DomainsListView(views.View):
    """
    Domains list view.
    """

    def get(self, request):
        domains = utils.get_existing_domains(request.ldap)
        admin = utils.get_admin(self.request.ldap)
        return render(request, 'pages/domains.html', locals())

class AddDomainView(FormView):
    """
    Form to add a new domain.
    """

    template_name = 'pages/domains-add.html'
    form_class    = AddDomainForm
    success_url   = reverse_lazy('domains')

    def get_form(self):
        """Return an instance of the form to be used in this view."""

        # Get users to display them on the webmaster select list
        self.request.ldap.search(
            settings.LDAP_TREE_USERS,
            settings.LDAP_FILTERS_USERS_WEBMASTER_FORM,
            attributes=['uid']
        )
        users = [ user.uid.value for user in self.request.ldap.entries ]
        self.request.ldap.search(
            settings.LDAP_TREE_USERS,
            settings.LDAP_FILTERS_SUDOERS,
            attributes=['uid']
        )
        superuser = self.request.ldap.entries[0].uid.value
        users.insert(0,superuser)

        # List of existing domains, for form validation
        domains = utils.get_existing_domains(self.request.ldap)
        domain_list = [ domain.vd.value for domain in domains ]
        domains_in_use = utils.domain_is_in_use(self.request.ldap)
        domains_used_list = [ domain.status.value for domain in domains_in_use]
        return AddDomainForm(users=users, domain_list=domain_list, domains_used_list=domains_used_list,**self.get_form_kwargs())

    def form_valid(self, form, **kwargs):
        """ Hook to be triggered if form is valid. """

        domain = form['name'].value()
        mail   = str(form['mail_server'].value()).upper()
        admin  = form['webmaster'].value()
        dkim   = form['dkim'].value()

        try:
            # Add domain
            dn = 'vd=%s,o=hosting,%s' % (domain, settings.LDAP_TREE_BASE)
            self.request.ldap.add(dn, ['VirtualDomain', 'top'], {
                'vd'            : domain,
                'adminid'       : admin,
                'accountActive' : mail,
                'lastChange'    : utils.unix_timestamp(),
                'delete'        : 'FALSE',
            })
            # Create postmaster
            postmaster_dn  = 'cn=postmaster,' + dn
            postmaster_pwd = utils.gen_pwd()
            self.request.ldap.add(postmaster_dn, ['VirtualMailAlias', 'top'], {
                'cn'            : 'Postmaster',
                'sn'            : 'Postmaster',
                'mail'          : 'postmaster@%s' % domain,
                'maildrop'      : 'postmaster',
                'accountActive' : mail,
                'creationDate'  : utils.ldap_creation_date(),
                'lastChange'    : utils.unix_timestamp(),
                'userpassword'  : hashed(HASHED_SALTED_SHA, postmaster_pwd)
            })
            # Add abuse email
            mail     = 'abuse@' + domain
            abuse_dn = 'mail=%s,%s' % (mail, dn)
            self.request.ldap.add(abuse_dn, ['VirtualMailAlias', 'top'], {
                'cn'            : 'abuse',
                'sn'            : 'abuse',
                'mail'          : mail,
                'maildrop'      : 'postmaster',
                'accountActive' : 'TRUE',
                'creationDate'  : utils.ldap_creation_date(),
                'lastChange'    : utils.unix_timestamp()
            })
            # DKIM
            if dkim:
                utils.add_dkim(self.request.ldap, domain)

            # If Rainloop installed check if con file exists, else creates it
            if 'rainloop' in self.request.enabled_services:
                utils.check_rainloop_conf(domain)
            messages.success(self.request, _('Dominio %s añadido con éxito' % domain))
        except Exception as e:
            utils.p("view_domains.py", "✕ There's was a problem creating the domain %s" % domain, e)
            messages.error(self.request, _('Ha habido un creando el dominio %s.' % domain))

        return super(AddDomainView, self).form_valid(form)

class EditDomainView(FormView):
    """
    Form to edit a new domain.
    """

    template_name = 'pages/domain.html'
    form_class    = EditDomainForm
    success_url   = reverse_lazy('domains')

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""

        self.domain_name = self.request.GET.get('domain')
        context = super(EditDomainView, self).get_context_data(**kwargs)
        context['domain'] = self.domain_name
        return context

    def get_form(self):
        """Return an instance of the form to be used in this view."""

        self.domain_name = self.request.GET.get('domain')
        domain      = utils.get_domain(self.request.ldap, self.domain_name)
        has_dkim    = utils.has_dkim(self.request.ldap, self.domain_name)
        kwargs = self.get_form_kwargs()
        kwargs['initial'] = {
            'webmaster'   : domain.adminid.value,
            'mail_server' : utils.ldap_val(domain.accountActive.value),
            'dkim'        : has_dkim,
            'old_dkim'    : has_dkim,
        }
        users = utils.get_users(self.request.ldap)
        return EditDomainForm(users=users, **kwargs)

    def form_valid(self, form):
        dn = 'vd=%s,%s' % (self.domain_name, settings.LDAP_TREE_HOSTING)
        webmaster = form['webmaster'].value()
        mail      = form['mail_server'].value()

        try:
            self.request.ldap.modify(dn, {
                'adminid'       : [( MODIFY_REPLACE, webmaster )],
                'accountActive' : [( MODIFY_REPLACE, utils.ldap_bool(mail) )],
            })
            dkim     = form['dkim'].value()
            old_dkim = bool(form['old_dkim'].value())
            if dkim != old_dkim:
                if dkim:
                    utils.add_dkim(self.request.ldap, self.domain_name)
                else:
                    utils.remove_dkim(self.request.ldap, self.domain_name)
            messages.success(self.request, _('Dominio modificado con éxito'))
        except Exception as e:
            messages.error(self.request, _('Ha habido un error modificando el dominio. '
                                           'Si el problema persiste contacta '
                                           'con los administrador-s'))
            p("✕ view_domains.py", "There was a problem updating the domain %s :" % self.domain_name, e)

        return super(EditDomainView, self).form_valid(form)

class MailmanDomains(views.View):

    def get(self, request, *args, **kwargs):
        active_mail_domains = utils.get_active_maildomains(request.ldap)
        mailman_domains     = utils.get_mailman_domains()
        active              = _('Activado')
        inactive            = _('Desactivado')
        processing          = _('Procesando')
        # Get dkim status in cpanel..if locked the puppet apply is going to be run
        # and dkim for mailman domains will be automatically creates
        #real_domains = utils.connect_to_postgre() 
        dkim_cpanel_status = utils.get_dkim_status_cpanel(request.ldap)
        for domain in mailman_domains:
            domain['dkim_status'] = False
            # Check vhost web status for given domain
            if domain['mail_host'] in active_mail_domains:
                domain['status'] = "<span class='fa fa-power-off enabled'></span> %s" % active
            else:
                domain['status'] = "<span class='fa fa-power-off disabled'></span> %s" % inactive
            # Check if dkim certificate exists in server 
            dkim_path = "/etc/opendkim/keys/%s/default.txt" % domain['mail_host']
            if os.path.isfile(dkim_path):
                domain['DKIM'] = "<span class='fa fa-power-off enabled'></span> %s" % active  
                # ou=aaa.com,ou=opendkim,ou=cpanel,dc=example,dc=tld
            # If dkim status is not ready, dkim is going to be created
            elif (not os.path.isfile(dkim_path) and dkim_cpanel_status != 'ready'):    
                domain['DKIM'] = "<span class='fa fa-spin fa-cog'></span> %s" % processing 
            # Run puppet local  to create dkim for mailman domains 
            else:
                domain['DKIM'] = "<span class='fa fa-power-off disabled'></span> "
                domain['dkim_status'] =True 
        return render(request, 'pages/mailman-domains.html', locals())

class CreateDkim(views.View):
    """ View to delete an entry from LDAP """

    def post(self, request):
        try:
            dn  = request.POST.get('dn')
            url = request.POST.get('url')
            # Create  item 
            utils.lock_dkim(self.request.ldap)
            messages.success(self.request, _('Creando certificado dkim para el dominio %s' % dn))
            return HttpResponseRedirect(url)
        except Exception as e:
            if settings.DEBUG:
                print(e)
            messages.error(self.request, _('Hubo algún problema creando el certifcado Dkim '
                                           'Contacta con los administrador-s si el problema persiste'))
            return HttpResponseRedirect(url)

