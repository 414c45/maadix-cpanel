# python
import urllib3, os, json, pwd

# django
from django.utils.translation import ugettext_lazy as _
from django import views
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse

# contrib
from ldap3 import MODIFY_REPLACE

# project
from .utils import get_release_info, connect_ldap, get_puppet_status, p, lock_cpanel, get_server_ip, check_domain_avalability
from django.conf import settings
from . import debug


class Services(views.View):
    """
    Apps list view.
    """

    def get_context_data(self, request):
        """ Set context data. """
        context = {
            'not_disabled' : [ 'mail' , 'mongodb', 'nodejs', 'docker' ],
        }
        try:
            release = {}
            if settings.NO_API:
                release = debug.RELEASE
            else:
                release = get_release_info(request)
            if release and 'groups' in release:
                available_services = release.get('groups')
                #print(available_services)
                request.ldap.search(
                    settings.LDAP_TREE_BASE,
                    settings.LDAP_FILTERS_INSTALLED_ENABLED_SERVICES,
                    attributes=['ou']
                )
                enabled_service_names = [ service.ou.value for service in request.ldap.entries ]
                context['enabled']    = [ service for service in available_services if service['id'] in enabled_service_names ]
                # TODO: Get groups with custom domain. Need to know their link
                request.ldap.search(
                    settings.LDAP_TREE_BASE,
                    settings.LDAP_FILTERS_INSTALLED_DISABLED_SERVICES,
                    attributes=['ou']
                )
                disabled_service_names = [ service.ou.value for service in request.ldap.entries ]
                context['disabled']    = [ service for service in available_services if service['id'] in disabled_service_names ]
        except Exception as e:
            p("Services view", "There was a problem retrieving enabled services", e)
        return context

    def get(self, request):
        """ Handle GET requests. """
        context = self.get_context_data(request)
        return render(request, 'pages/services.html', context)

    def post(self, request):
        """ Handle POST requests. """
        context = self.get_context_data(request)
        return render(request, 'pages/services.html', context)


class ServicesAvailable(views.View):
    """
    View for installing apps.
    """

    def service_install_description(service):

        return descriptions.get(service, 'default')

    def get(self, request):
        context = {
            'descriptions' : {
                'domain' : _("Esta aplicación necesita ser instalada bajo un dominio o "
                           "subdominio propio. Inserta un dominio válido que no esté "
                           "siendo usado por ninguna otra aplicación y cuyos dns estén "
                           "ya apuntando a la IP de este servidor:%s") % get_server_ip(),
                'email'  : _("Esta aplicación necesita una cuenta de email asociada. "
                            "Inserta una dirección de correo electrónico válida:"),
                'default' : _("Inserta un texto"),
            }
        }
        release = {}
        puppet_status = {}
        if settings.NO_API:
            release_info  = debug.RELEASE
            puppet_status = debug.STATUS
        else:
            release_info  = get_release_info(request)
            puppet_status = get_puppet_status(request)
        context['status'] = puppet_status
        context['release'] = release_info['release']
        try:
            if 'groups' in release_info:
                # Get all available services, installed or not
                all_services = release_info.get('groups')
                # Get installed services
                request.ldap.search(
                    settings.LDAP_TREE_BASE,
                    settings.LDAP_FILTERS_INSTALLED_SERVICES_ALL,
                    attributes=['ou']
                )
                installed_services = [ service.ou.value for service in request.ldap.entries ]
                # Exclude installed services from available services
                context['available'] = [ service for service in all_services if service['id'] not in installed_services ]
                # Use this in development to show all apps, even if they are already installed (for Marta)
                #context['available'] = [ service for service in all_services if service['id']]
                if not context['available']:
                     context['all_installed']  = True
        except Exception as e:
        # Maybe mark a difference between no available aplicaction due to 'All of them have been installed' and a 
        # problem retreiving available application from ldap or whatever error
        # Now both cases are the same and sets context['no_updates']  = True
            context['no_updates']  = True
            p("ServicesAvailable view", "✕ There was a problem retrieving enabled services", e)
        #if puppet_status.get('status') == 'error' or puppet_status.get('status') == 'pending':
        if puppet_status['puppetstatus'] == 'error' or puppet_status['puppetstatus'] == 'pending':
            context['maintenance'] = True
        return render(request, 'pages/services-install.html', context)


class ServicesUpdate(views.View):
    """ View to enable/disable services via AJAX"""

    def post(self, request):
        try:
            services = json.loads(request.body.decode('utf-8'))
            # update services to be disabled
            for service, value in services['disable'].items():
                if value:
                    dn = 'ou=%s,%s' % (service, settings.LDAP_TREE_SERVICES)
                    request.ldap.modify(dn, {
                        'type'   : [(MODIFY_REPLACE, 'installed')],
                        'status' : [(MODIFY_REPLACE, 'disabled')]
                    })
            # update services to be enabled
            for service, value in services['enable'].items():
                if value:
                    dn = 'ou=%s,%s' % (service, settings.LDAP_TREE_SERVICES)
                    request.ldap.modify(dn, {
                        'type'   : [(MODIFY_REPLACE, 'available')],
                        'status' : [(MODIFY_REPLACE, 'enabled')]
                    })
            # lock cpanel
            lock_cpanel(request)

            #messages.success(self.request, _('Aplicaciones modificadas con éxito'))
            return HttpResponseRedirect( reverse('logout') )

        except Exception as e:
            messages.error(self.request, _('Hubo algún problema modificando el estado de tus aplicaciones. '
                                           'Contacta con los administrador-s si el problema persiste'))
            return HttpResponse("Something failed trying to update services in LDAP", status=500)

class ServicesCheckinstall(views.View):
    def post(self, request):
        errors = 0;
        data         = json.loads(request.body.decode('utf-8'))
        syschek    = data.get('marked_system')
        dependencies = data.get('marked_deps_indupt')
        response_data = {}
        response_data["app"]=[]
        existing_user='';
        # Check if application user already exists
        for sys in syschek:
            # Check if user need for the application already exists
            if (sys["name"] == 'sysuser'):
                try:
                    existing_user = pwd.getpwnam(sys["value"])
                except Exception as e:
                    p("✕ view_users.py", "User does not exist.: ", e)
                if existing_user:
                    errors=1
                    response_data['error-sysuser'] = _('Ya existe el usuario %s. No se puede instalar la aplicación' % sys["value"])
                    args = {
                            "service" : sys["service"],
                            "name" : sys["value"],
                            "errortype" : "sysuser",
                            "errormsg" : _('Ya existe el usuario %s. Eliminalo para poder proceder' % sys["value"])
                        }
                    response_data["app"].append(args)            
        for dep in dependencies:
            if (dep["name"] == 'domain'):
                message = check_domain_avalability(request.ldap,dep["value"])  
                #response_data['error-domain']= message
                if message:
                    errors = 1
                    values = { 
                            "service" : dep["service"],
                            "name" : dep["value"],
                            "errortype" : "domain",
                            "errormsg" : message
                        }
                    response_data["app"].append(values)
        response_data["errors"] =  errors 
        response_data["errorsmsg"] = _("Corrige los errores marcados en rojo")
        #json_data = json.dumps(response_data)
        return JsonResponse (response_data)        

class ServicesInstall(views.View):
    """ View to install services via AJAX"""
    # TODO: 
    # If a service has dependency sysuser, check that this usr doesn't exists
    # if dependency is 'domain' check that this domain is not in use by another aplicaction, or created as virtualhost
    # or present in dom in this same view (assigne to two apps at a time)
    # If none of these cases giver true, check DNS record A

    def post(self, request):
        data         = json.loads(request.body.decode('utf-8'))
        services     = data.get('services')
        #dependencies = data.get('dependencies')
        #sysusers    = data.get('marked_system')
        dependencies = data.get('marked_deps_indupt')
        try:
            # Install services
            # need to check if service has a custom input, such us custom domain, paassword text etc
            # If so update correspondent fields
            for service in services:
                if settings.DEBUG_INSTALL:
                    print("Installing service ", service)
                else:
                    try:
                        dn = "ou=%s,%s" % (service, settings.LDAP_TREE_SERVICES)
                        request.ldap.search(
                            dn,
                            "(objectclass=*)",
                            attributes = [ 'dn']
                        )
                        if request.ldap.entries:
                            request.ldap.modify(dn, {'status' : [(MODIFY_REPLACE, 'install')]})
                        else:
                            p("ServicesInstall view", "✕ service Nt found", e)
                    except Exception as e:
                        try:
                            self.request.ldap.add(dn, [
                                'organizationalUnit',
                                'metaInfo',
                            ], {
                                'ou' : service,
                                'status' : 'install',
                                'type'   : 'available',
                            })
                        except Exception as e:
                            p("ServicesInstall view", "✕ could not add service ", e)
            # Add dependencie
            # This are user input dependencies, that are inserted as subtree of the app they belong
            for dependency in dependencies:
                create_dep = False
                if settings.DEBUG_INSTALL:
                    print("Installing dependency ", dependency)
                else:
                    try:
                        dn = 'ou=%s,ou=%s,%s' % (dependency["name"], dependency["service"], settings.LDAP_TREE_SERVICES);
                        request.ldap.search(
                            dn,
                            "(objectclass=*)",
                            attributes = [ 'dn']
                        )
                        if request.ldap.entries:
                            request.ldap.modify(dn, {'status' : [(MODIFY_REPLACE, dependency["value"])]})
                        else:
                            p("ServicesInstall view", "✕ entry for service deps does not exists", e)
                    except Exception as e:
                        try:
                            self.request.ldap.add(dn, [
                                'organizationalUnit',
                                'metaInfo',
                            ], {
                                'ou'     : dependency["name"],
                                'status' : dependency["value"],
                            })
                        except Exception as e:
                            p("ServicesInstall view", "✕ Could not create dependency tree", e)

            if not settings.DEBUG_INSTALL:
                lock_cpanel(request)
           # messages.success(self.request, _('Aplicaciones instaladas con éxito'))
            return HttpResponseRedirect( reverse('logout'))

        except Exception as e:
            p("ServicesInstall view", "✕ There was a problem installing services", e)
            messages.error(self.request, _('Hubo algún problema modificando el estado de tus aplicaciones. '
                                           'Contacta con los administrador-s si el problema persiste'))
            return HttpResponseRedirect( reverse('services-available') )
