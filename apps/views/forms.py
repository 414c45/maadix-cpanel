# python
import re, base64, pwd 
# django
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse, reverse_lazy
from django.contrib.auth.password_validation import validate_password
# contrib
from ldap3 import MODIFY_REPLACE, HASHED_SALTED_SHA
from ldap3.utils.hashed import hashed
# project
from django.conf import settings
from . import widgets, utils


class GenericForm(forms.Form):
    """
    Generic form to be inherited by regular forms on the site.
    """

    required_css_class = 'required'
    error_css_class    = 'error'

    #  Clean method that check if passwords coincide
    #  In case the form doesn't have both password fields
    #  it will work because both fields will be None
    def clean(self):
        cleaned_data = super(GenericForm, self).clean()
        password           = cleaned_data.get("password")
        confirmed_password = cleaned_data.get("password_2")
        if password and not confirmed_password:
            raise forms.ValidationError(
                _("Por favor confirma la contraseña que has introducido.")
            )
        if password != confirmed_password:
            raise forms.ValidationError(
                _("Las contraseñas no coinciden.")
            )
        if password:
            validate_password(password)


    #  Render fields as divs
    #  and adds a css class to these divs, as we've removed %(html_class_attr)s
    #  this rendering method won't include any class defined by field in the form
    def as_div(self):
        "Return this form rendered as HTML <div>s."
        return self._html_output(
            normal_row='<div class="form__field"> %(label)s %(help_text)s %(field)s</div>',
            error_row='%s',
            row_ender='</div>',
            help_text_html=' <p class="form__help-text">%s</p>',
            errors_on_separate_row=True
        )


class CustomLoginForm(forms.Form):
    """ Custom login form """

    username = forms.CharField(label='Usuario', required=True)
    password = forms.CharField(label='Contraseña', widget=forms.PasswordInput(), required=True)

    def as_p(self):
        "Return this form rendered as HTML <p>s."
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(field)s %(label)s %(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True)


class PasswordRecoveryForm(GenericForm):
    """ Form to add a new email account """

    username = forms.CharField(label=_('Nombre de usuario'), required=True,
                               help_text=_("Inserta el nombre de usuario del administrador del panel de control"))
    email    = forms.EmailField(label=_('Correo electrónico'), required=True,
                                help_text=_('Inserta la cuenta de correo electrónico asociada al usuario administrador'))

    def __init__(self, *args, **kwargs):
        self.admin_name = kwargs.pop('admin_name')
        self.admin_email = kwargs.pop('admin_email')
        super(PasswordRecoveryForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(GenericForm, self).clean()
        name        = cleaned_data.get("username")
        email       = cleaned_data.get("email")
        if name != self.admin_name or email != self.admin_email:
            raise forms.ValidationError(
                _("Credenciales de administrador no válidas.")
            )

    def as_p(self):
        "Return this form rendered as HTML <p>s."
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(field)s %(label)s %(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True)

class PasswordResetForm(GenericForm):
    """ Form to add a new email account """

    username   = forms.CharField(label=_('Nombre de usuario'), required=True,
                               help_text=_("Inserta el nombre de usuario del administrador del panel de control"))
    code       = forms.CharField(label=_('Código de verificación'), required=True,
                                help_text=_('Inserta el código de verificación que hemos mandado a tu correo'))
    password   = forms.CharField(label=_('Nueva contraseña'), widget=forms.PasswordInput(), required=True)
    password_2 = forms.CharField(label=_('Vuelve a introducir la contraseña'), widget=forms.PasswordInput(), required=True)

    def clean(self):
        cleaned_data = super(GenericForm, self).clean()
        user         = cleaned_data.get("username")
        code         = cleaned_data.get("code")
        if user != self.user or code != self.code:
            raise forms.ValidationError(
                _("Credenciales de administrador no válidas.")
            )

    def __init__(self, *args, **kwargs):
        self.user  = kwargs.pop('user')
        self.code  = kwargs.pop('code')
        super(PasswordResetForm, self).__init__(*args, **kwargs)

    def as_p(self):
        "Return this form rendered as HTML <p>s."
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(field)s %(label)s %(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True)

class ActivateForm(GenericForm):
    """ Form to edit user's profile. """

    username         = forms.CharField(label=_('Nombre de usuaria'), required=False)
    current_password = forms.CharField(label=_('Contraseña actual'), widget=forms.PasswordInput(), required=True)
    password         = forms.CharField(label=_('Nueva contraseña'), widget=forms.PasswordInput(), required=True)
    password_2       = forms.CharField(label=_('Vuelve a introducir la contraseña'), widget=forms.PasswordInput(), required=True)
    email            = forms.EmailField(label=_('Correo electrónico'), required=True,
                                        help_text=_('Asegúrate de que el correo electrónico asociado a tu cuenta sea válido y '
                                                    'que tengas acceso a él: si pierdes la contraseña únicamente podrás '
                                                    'resetearla a través de este correo electrónico.'))
    sudousername         = forms.CharField(label=_('Nombre de usuaria sudo'), required=False)
    sudopassword         = forms.CharField(label=_('Nueva contraseña'), widget=forms.PasswordInput(), required=True)
    sudopassword_2       = forms.CharField(label=_('Vuelve a introducir la contraseña'), widget=forms.PasswordInput(), required=True)


    def __init__(self, *args, **kwargs):
        #self.old_pwd  = kwargs.pop('pwd')
        #self.sudouser = kwargs.pop('sudouser')
        super(ActivateForm,self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs = {
            'readonly' : True,
            'class'    : 'disabled'
        }
        self.fields['sudousername'].widget.attrs = {
            'readonly' : True,
            'class'    : 'disabled'
        }

    def clean(self):
        cleaned_data = super().clean()
        username = self.cleaned_data.get('username')
        sudousername = self.cleaned_data.get('sudousername')
        current_pwd  = self.cleaned_data.get('current_password')
        new_password = self.cleaned_data.get('password')
        new_password_2 = self.cleaned_data.get('password_2')
        sudo_password = self.cleaned_data.get('sudopassword')
        sudo_password_2 = self.cleaned_data.get('sudopassword_2')
        # Current password  match doesn't need to be checked as it is used for bind
        # Just check if password confirmation matches
        """
        if current_pwd and current_pwd != self.old_pwd:
            raise forms.ValidationError(
                _("La contraseña actual no coincide con la introducida.")
            )
        """
        ldap = utils.connect_ldap(username, current_pwd)
        if ldap['connection']: 
            if current_pwd == new_password:
                raise forms.ValidationError(
                    _("La nueva contraseña es igual a la anterior. Por razones de seguridad debes cambiarla" )
                ) 
            if current_pwd and not new_password:
                raise forms.ValidationError(
                    _("Introduce la nueva contraseña para la cuenta %s" % username)
                )
            if current_pwd and not sudo_password:
                raise forms.ValidationError(
                    _("Introduce la nueva contraseña para la cuenta %s" % sudousername)
                )
            if sudo_password and not sudo_password_2:
                raise forms.ValidationError(
                    _("Confirma la contraseña para la cuenta %s" % sudousername)
                )
            if sudo_password != sudo_password_2:
                raise forms.ValidationError(
                    _("Las contraseñas de la cuenta %s no coinciden" % sudousername)
                )
            if new_password and sudo_password:
                validate_password(new_password)
                validate_password(sudo_password)
        
        elif ldap['error'] == 'LDAPSocketOpenError':
            # If socket error, ldap is shut down for some reasons
            # warn the user accordingly
            raise forms.ValidationError(
                _("Error de conexión a la base de datos")
            )

        elif ldap['error'] == 'LDAPInvalidCredentialsResult':
            raise forms.ValidationError(
                _("La contraseña actual que has insertado no es correcta")
            )


class ProfileForm(GenericForm):
    """ Form to edit user's profile. """

    username         = forms.CharField(label=_('Nombre de usuaria'), required=False)
    current_password = forms.CharField(label=_('Contraseña actual'), widget=forms.PasswordInput(), required=False)
    password         = forms.CharField(label=_('Nueva contraseña'), widget=forms.PasswordInput(), required=False)
    password_2       = forms.CharField(label=_('Vuelve a introducir la contraseña'), widget=forms.PasswordInput(), required=False)
    email            = forms.EmailField(label=_('Correo electrónico'), required=True,
                                        help_text=_('Asegúrate de que el correo electrónico asociado a tu cuenta sea válido y '
                                                    'que tengas acceso a él: si pierdes la contraseña únicamente podrás '
                                                    'resetearla a través de este correo electrónico.'))

    def __init__(self, *args, **kwargs):
        self.old_pwd  = kwargs.pop('pwd')
        role          = kwargs.pop('role')
        super(ProfileForm,self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs = {
            'readonly' : True,
            'class'    : 'disabled'
        }
        if role != 'admin':
            del self.fields['email']

    def clean(self):
        cleaned_data = super().clean()
        current_pwd  = self.cleaned_data.get('current_password')
        new_password = self.cleaned_data.get('password')
        if current_pwd and current_pwd != self.old_pwd:
            raise forms.ValidationError(
                _("La contraseña actual no coincide con la introducida.")
            )
        if current_pwd and not new_password:
            raise forms.ValidationError(
                _("Introduce la nueva contraseña.")
            )


class AddDomainForm(GenericForm):
    """ Form to add a new domain """

    name_help_text = _("Inserta un nombre de dominio válido (o un subdominio). "
                     "Para los dominios activados podrás crear aplicaciones web "
                     "que estarán disponibles desde cualquier navegador visitando "
                     "https://tudominio.com. El certificado SSL que activa el protocolo "
                     "seguro HTTPS se activará automáticamente si la configuración de DNS es correcta")
    mail_help_text = _("Activa la siguiente casilla si quieres que el correo electrónico "
                      "para este dominio sea gestionado por este servidor. Si el correo está "
                      "gestionado por otro servidor (por ejemplo el mismo proveedor de dominio), "
                      "deja esta casilla desactivada. Podrás cambiar esta opción en cualquier momento "
                      "desde la página de edición del dominio.")
    webmaster_help_text = _("Por cada dominio que actives en este panel, se creará una carpeta con su "
                           "mismo nombre en /var/www/html/ en la que puedes subir tu aplicación web. "
                           "Puedes selecionar un Webmaster entre las cuentas de usuarios ordinarios "
                           " que tengan activado el servicio SFTP y que tendrá así permisos para crear, "
                            "borrar o modificar archivos dentro de esta carpeta.<br>"
                           "Los usuarios Webmaster, excluido el Superusario, solo tendrán acceso a las carpetas "
                           "que les han sido asignadas.")
    dkim_help_text = _("Advertencia: si activas la siguiente casilla tendrás que incluir el registro "
                      "DKIM en los DNS de tu dominio. De lo contrario, haber generado la clave DKIM sin "
                      "incluir el registro correspondiente en los DNS puede generar problemas de entrega "
                      "de tu correo electrónico. Para saber más sobre las ventajas de activar DKIM, "
                      "haz click <a href='/domains/instructions' target='_blank'>aquí</a>.")


    name        = forms.CharField(label=_('Nombre de dominio'),
                                 max_length=100, help_text=name_help_text, required=True)
    mail_server = forms.BooleanField(label=_('Activar servidor de correo para este dominio'),
                                     help_text=mail_help_text, required=False,
                                     widget=widgets.LabelledCheckbox(label=_('Activar servidor de correo')))
    webmaster   = forms.ChoiceField(label=_('Webmaster (Administrador del sitio web)'),
                                    help_text=webmaster_help_text)
    dkim        = forms.BooleanField(label=_('Activar DKIM para este dominio'),
                                     help_text=dkim_help_text, required=False,
                                     widget=widgets.LabelledCheckbox(label=_('Activar DKIM')))

    def clean(self):
        cleaned_data = super().clean()
        name  = self.cleaned_data.get('name')
        mail_server = self.cleaned_data.get("mail_server")
        if not re.match("^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,20}$", name):
            raise forms.ValidationError(
                _("%s no en un nombre de dominio válido" % name)
            )
        if name in self.domains:
            raise forms.ValidationError(
                _("El dominio %s ya está activado" % name)
            )
        if name in utils.get_server_host():
            raise forms.ValidationError(
                _("%s es el dominio actual del sistema" % name)
          )
        if name in self.domains_in_use:
            raise forms.ValidationError(
                _("El dominio %s está en uso por otra aplicación" % name)
          )
        mailman_domains = utils.get_mailman_domains()
        for mdomain in mailman_domains:
            if name == mdomain['mail_host'] and mail_server:
                raise forms.ValidationError(
                    _(" No puedes activar el servidor de correo para el dominio %s. Ya  está en uso por la aplicación Mailman." % name)
              )
    def __init__(self, *args, **kwargs):
        users = kwargs.pop('users')
        self.domains = kwargs.pop('domain_list')
        self.domains_in_use = kwargs.pop('domains_used_list')
        super(AddDomainForm,self).__init__(*args, **kwargs)
        self.fields['webmaster'].choices = ((user, user if user != "admin" else _("%s (Superusuario)" % user)) for user in users)
        self.fields['webmaster'].choices.insert(0,(None, _('Asignar Webmaster')))
        self.fields['name'].widget.attrs['pattern'] = "^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,20}$"

class EditDomainForm(GenericForm):
    """ Form to add a new domain """


    webmaster   = forms.ChoiceField(label=_('Editar webmaster'), help_text='')
    mail_server = forms.BooleanField(label=_('Activar servidor de correo para este dominio'),
                                     help_text=_('Activa esta casilla si quieres que el correo '
                                     'electrónico para este dominio sea gestionado por este servidor. '
                                     'Recuerda que el registro MX de los DNS tendrá que ser cmdev.maadix.org. '
                                     'Puedes averiguar cual es la configuración de DNS actual haciendo click aquí.'),
                                     widget=widgets.LabelledCheckbox(label=_('Activar servidor de correo')), required=False)
    dkim        = forms.BooleanField(label=_('Activar DKIM para este dominio'),
                                     help_text=_('Activa esta casilla si quieres activar la clave DKIM. Puedes '
                                     'averiguar cual es la configuración de DNS necesaria para usar DKIM haciendo '
                                     'click aquí.'),
                                     widget=widgets.LabelledCheckbox(label=_('Activar DKIM')), required=False)
    old_dkim    = forms.BooleanField(widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        users = kwargs.pop('users')
        super(EditDomainForm, self).__init__(*args, **kwargs)
        self.fields['webmaster'].choices = ((user, user) for user in users)

class EmailForm(GenericForm):
    """ Form to add a new email account """

    email      = forms.CharField(label=_('Usuario'), required=True,
                                 help_text=_("Introduce tu nombre de usuario. Por ejemplo si quieres tener un correo "
                                             "'user@ejemplo.com' tu nombre de usuario sería 'user'"))
    at         = forms.CharField(label="",required=False)
    domain     = forms.ChoiceField(label=_('Dominio'), required=True,
                                   help_text=_("Introduce el dominio del correo. En el ejemplo anterior "
                                               "escogerías 'ejemplo.com'"))
    password   = forms.CharField(label=_('Contraseña'), widget=forms.PasswordInput(), required=True)
    password_2 = forms.CharField(label=_('Vuelve a introducir la contraseña'), widget=forms.PasswordInput(), required=True)
    name       = forms.CharField(label=_('Nombre'), required=True)
    surname    = forms.CharField(label=_('Apellidos'), required=True)

    def clean(self):
        super().clean()
        email = self.cleaned_data.get("email")
        if not re.match("^[\w]{1,}[\w.+-_]{0,}$", email):
            raise forms.ValidationError(
                _("El nombre que has escogido usa caracteres no permitidos.")
            )

    def __init__(self, *args, **kwargs):
        domains = kwargs.pop('domains')
        super(EmailForm,self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs = {
            'pattern' : '^[\w]{1,}[\w.+-_]{0,}'
        }
        self.fields['domain'].choices = ((domain, domain) for domain in domains)
        self.fields['at'].widget.attrs['readonly'] = True


class EditEmailForm(GenericForm):
    """ Form to add a new email account """

    forward_help_text  = _("Puedes reenviar los correos electrónicos entrantes a "
                         "una o más cuentas de correo alternativas. Si quieres "
                         "que se reenvíen a múltiples cuentas, separa cada una "
                         "de ellas con una coma (usuario1@example.com,usuario2@example.com. "
                         "Recuerda que si quieres seguir recibiendo una copia de los "
                         "correos entrantes en tu cuenta actual admin@example.com, "
                         "también tendrás que incluirla en el listado.")

    givenName      = forms.CharField(label=_('Nombre'), required=False)
    sn             = forms.CharField(label=_('Apellidos'), required=False)
    password       = forms.CharField(label=_('Contraseña'), widget=forms.PasswordInput(),
                                         required=False)
    password_2     = forms.CharField(label=_('Vuelve a introducir la contraseña'),
                                         widget=forms.PasswordInput(), required=False)
    forwardActive  = forms.BooleanField(label=_("Reenvío automático"), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Activar renvío automático')))
    maildrop       = forms.CharField(label=_('Cuentas de destino para el reenvío automático'),
                                         help_text=forward_help_text, required=False)
    vacationActive = forms.BooleanField(label=_("Respuesta automática"), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Activar respuesta automática')))
    vacationinfo   = forms.CharField(label=_('Mensaje de respuesta automática'), required=False,
                                     help_text=_('Inserta el texto del mensaje de respuesta automática.'))

    def clean(self):
        super().clean()
        maildrop = self.cleaned_data.get("maildrop")
        emails   = maildrop.split(",")
        for email in emails:
            email = email.strip()
            if email and not re.match("^[a-z0-9]+\@[a-z0-9]+\.[a-z0-9]+$", email):
                raise forms.ValidationError(
                    _("Parece que uno de los correos que has introducido no es válido.")
                )


    def __init__(self, *args, **kwargs):
        super(EditEmailForm,self).__init__(*args, **kwargs)
        self.fields['forwardActive'].widget.attrs = { 'data-link-display' : 'id_maildrop' }
        self.fields['vacationActive'].widget.attrs = { 'data-link-display' : 'id_vacationinfo' }


class UserForm(GenericForm):
    """ Form to add a new email account """

    sftp_help_text      = "Requerido para que los usuarios webmaster puedan editar los archivos."
    sftp_help_text_full = """Requerido para que los usuarios webmaster puedan editar los archivos.
                             Para poder desactivarlo necesitas antes quitarle el rol de webmaster
                             para los siguientes dominios: """

    username       = forms.CharField(label=_('Nombre de usuario'), required=True)
    name           = forms.CharField(label=_('Nombre'), required=False)
    surname        = forms.CharField(label=_('Apellidos'), required=False)
    email          = forms.EmailField(label=_('Correo electrónico'), required=True,
                                      help_text=_("Introduce un correo electrónico válido. "
                                                  "Este campo se autocompletará con correos existentes en el sistema. "
                                                  "Si quieres ver la lista completa haz doble clic en el campo de texto (vacío)."))
    password       = forms.CharField(label=_('Contraseña'), widget=forms.PasswordInput(), required=True)
    password_2     = forms.CharField(label=_('Vuelve a introducir la contraseña'),
                                     widget=forms.PasswordInput(), required=True)
    sshd           = forms.BooleanField(label=_('Activar acceso SFTP'), required=False,
                                        help_text=sftp_help_text,
                                        widget=widgets.LabelledCheckbox(label=_('Activar acceso SFTP')))
    home_dir       = forms.CharField(label='Directorio personal', required=False)
    openvpn       = forms.BooleanField(label=_('Activar cuenta VPN'), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Activar cuenta VPN')))
    instructions   = forms.BooleanField(label=_('Instrucciones de configuración VPN'), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Enviar instrucciones')),
                                        help_text=_("Envía al usuario un correo con los archivos de configuración y las instrucciones "
                                                    "para configurar el cliente VPN. <br/>Advertencia: Las instrucciones incluyen todos los "
                                                    "datos necesarios menos la contraseña, que por razones de seguridad debes proporcionar "
                                                    "al usuario por otro canal."))
    apache         = forms.BooleanField(label=_('Activar PHPMyAdmin'), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Activar PHPMyAdmin')))

    def clean(self):
        super().clean()
        username     = self.cleaned_data.get("username")
        openvpn     = self.cleaned_data.get("openvpn")
        instructions = self.cleaned_data.get("instructions")
        email        = self.cleaned_data.get("email")
        existing_user = False
        # check if user already exists
        # when creating a new user
        if not self.is_edit_form:
            # if user is not present in ldap, also check in system before validate
            try:
                existing_user = pwd.getpwnam(username)
            except Exception as e:
                utils.p("✕ view_users.py", "User does not exist.: ", e)
            if existing_user:
                raise forms.ValidationError(
                    _("Ya existe un usuario con ese nombre. Has de escoger un nombre de usuario distinto.")
                )
                context['display_form'] = True
        # check if there's email to send notifications if checked
        if instructions and not email:
        ### It should never happen....email is a required field !!!!!!
            raise forms.ValidationError(
                _("Para poder recibir las instrucciones de configración de la VPN, es necesaria una dirección de correo. "
                  "Introduce una dirección de correo o desactiva el envío de instrucciones.")
            )
            context['display_form'] = True
        if not openvpn:
            self.cleaned_data['instructions'] = False
        return {} 


    def __init__(self, *args, **kwargs):
        emails       = kwargs.pop('emails')
        self.is_edit_form  = kwargs.pop('edit_form')
        # Store ldap in the form itself to make it accesible from clean method
        # only when creating a new user
        domains = []
        if 'domains' in kwargs:
            domains = kwargs.pop('domains')
        available_services = kwargs.pop('services')
        super(UserForm,self).__init__(*args, **kwargs)
        self.fields['email'].widget = widgets.ListTextWidget(
            data_list=emails,
            name='mail',
            attrs={
                'autocomplete':'off'
            }
        )
        self.fields['openvpn'].widget.attrs = {
            'data-link-display' : 'id_instructions'
        }

        # if the user is webmaster disable sftp field and
        # show a different help_text
        if domains:
            self.sftp_help_text = self.sftp_help_text_full
            for domain in domains:
                self.sftp_help_text += "%s, " % domain
            self.fields['sshd'].widget.attrs = {
                'readonly' : True,
                'class'    : 'disabled'
            }
            self.fields['sshd'].help_text = _(self.sftp_help_text)

        # if the user hasn't a sftp account don't show home_dir
        self.fields['home_dir'].widget.attrs = {
            'readonly' : True,
            'class'    : 'disabled'
        }
        sshd = kwargs.get('initial').get('sshd')
        if not sshd:
            del self.fields['home_dir']

        # if openvpn is not activated in the server hide related_fields
        if not 'openvpn' in available_services:
            del self.fields['openvpn']
            del self.fields['instructions']
        # if phpmyadmin is not activated in the server hide related_fields
        if not 'phpmyadmin' in available_services:
            del self.fields['apache']

        # if editing a user instead of creating it
        if self.is_edit_form:
            # dont force to input password again if not needed
            self.fields['password'].required   = False
            self.fields['password_2'].required = False
            # set username as a hidden and disabled field
            self.fields['username'].widget = forms.HiddenInput({
                'attrs' : {
                    'readonly' : True,
                    'class'    : 'disabled',
                }
            })
        elif 'home_dir' in self.fields:
            del self.fields['home_dir']

class SuperuserForm(GenericForm):
    """ Form to edit superuser account """

    username         = forms.CharField(label=_('Nombre de usuario no editable (Para autenticación SFTP/SSH)'), required=True)
    name             = forms.CharField(label=_('Nombre'), required=False)
    surname          = forms.CharField(label=_('Apellidos'), required=False)
    email            = forms.EmailField(label=_('Correo electrónico'), required=True,
                                      help_text=_("Introduce un correo electrónico válido. "
                                                  "Este campo se autocompletará con correos existentes en el sistema. "
                                                  "Si quieres ver la lista completa haz doble clic en el campo de texto (vacío)."))
    current_password = forms.CharField(label=_('Introduce tu contraseña de admin'), widget=forms.PasswordInput(), required=False)
    password         = forms.CharField(label=_('Contraseña nueva del superusuario'), widget=forms.PasswordInput(), required=False)
    password_2       = forms.CharField(label=_('Vuelve a introducir la contraseña nueva del superusuario'), widget=forms.PasswordInput(), required=False)
    home_dir         = forms.CharField(label=_('Directorio personal'), required=False)
    openvpn          = forms.BooleanField(label=_('Activar cuenta VPN'), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Activar VPN')))
    instructions     = forms.BooleanField(label=_('Instrucciones de configuración VPN'), required=False,
                                        widget=widgets.LabelledCheckbox(label=_('Enviar instrucciones')),
                                        help_text=_("Envía al usuario un correo con los archivos de configuración y las instrucciones "
                                                    "para configurar el cliente VPN. <br/>Advertencia: Las instrucciones incluyen todos los "
                                                    "datos necesarios menos la contraseña, que por razones de seguridad debes proporcionar "

                                                    "al usuario por otro canal."))
    apache           = forms.BooleanField(label=_('Activar PHPMyAdmin'), required=False,
                                                    widget=widgets.LabelledCheckbox(label=_('Activar PHPMyAdmin')))

    def __init__(self, *args, **kwargs):
        emails = kwargs.pop('emails')
        available_services = kwargs.pop('services')
        self.old_pwd  = kwargs.pop('password')
        super(SuperuserForm,self).__init__(*args, **kwargs)
        self.fields['email'].widget = widgets.ListTextWidget(
            data_list=emails,
            name='mail',
            attrs={'autocomplete':'off'}
        )
        self.fields['openvpn'].widget.attrs = {
            'data-link-display' : 'id_instructions'
        }
        self.fields['username'].widget.attrs['readonly'] = True
        self.fields['username'].widget.attrs['class'] = 'disabled'
        self.fields['home_dir'].widget.attrs['readonly'] = True
        self.fields['home_dir'].widget.attrs['class'] = 'disabled'
        if not 'openvpn' in available_services:
            del self.fields['openvpn']
            del self.fields['instructions']
        if not 'phpmyadmin' in available_services:
            del self.fields['apache']

    def clean(self):
        super().clean()
        current_pwd  = self.cleaned_data.get('current_password')
        new_password = self.cleaned_data.get('password')
        if not current_pwd and new_password:
            raise forms.ValidationError(
                _("Introduce tu contraseña de administrador")
            )
        if current_pwd and not new_password:
            raise forms.ValidationError(
                _("Introduce la nueva contraseña del superusuario")
            )
        if current_pwd and current_pwd != self.old_pwd:
            raise forms.ValidationError(
                _("Parece que no has introducido correctamente tu contraseña de administrador")
            )

class PostmasterForm(GenericForm):
    """ Form to edit postmaster account """

    username   = forms.CharField(label=_('Nombre de usuario'), required=True)
    password   = forms.CharField(label=_('Contraseña'), widget=forms.PasswordInput(), required=True)
    password_2 = forms.CharField(label=_('Vuelve a introducir la contraseña'), widget=forms.PasswordInput(), required=True)

    def __init__(self, *args, **kwargs):
        super(PostmasterForm,self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['readonly'] = True
        self.fields['username'].widget.attrs['class'] = 'disabled'


class NotificationForm(GenericForm):
    """ Form to add a new email account """
    log_help_text =""
    log_custom_help_text = _("Tu sistema envia periodcamente correos con información "
                              "sobre su estado como fallos de los servicios, errores o actualizaciones (Logs del sistema).<br>"
                              "Si esta opción está activada recibirás los Logs del sistema "
                              "a la cuenta de correo electrónico asoicada al administrador del panel de control.<br>"
                              "De lo contrario, serán enviados al equipo técnico de MaadiX.<br><br>"
                              "Si cambias esta configuración se bloqueará el acceso al panel de control durante unos minutos. "
                              "Todos los usuarios que tengan una sesión activa serán forzados a salir y redireccionados a una página "
                              "en la que se mostrará el estado de la operación. Cuando el proceso termine, se activará el formulario para volver a acceder." )

    email_help_text = _("Puedes cambiar esta configuración y elegir una de las cuentas de correo activadas "
                        "en tu sistema para que sea el remitente de las notificaciones. "
                        "Para cambiar este valor, elige un correo electrónico disponible en el listado") 
    email = forms.ChoiceField(label=_('Remitene de las notificaciones'), required=True,help_text=email_help_text)
    log_server = forms.BooleanField(label=_('Configurar destinatario de Logs'),
                                     help_text = log_custom_help_text, required=False,
                                     widget=widgets.LabelledCheckbox(label=_('Recibir Logs')))

    def __init__(self, *args, **kwargs):
        emails = kwargs.pop('emails')
        # Store this value to compare in valid_form if it has changed
        # if so, a lock_panel is needed
        #self.log_mail_status = kwargs.pop('log_mail_status')
        super(NotificationForm,self).__init__(*args, **kwargs)
        self.fields['email'].choices = [ (email,email) for email in emails ]
        #self.fields['email'].choices.insert(0,(None, _('Selecciona un correo')))

class FqdnForm(GenericForm):
    """ Form to add a new domain """

    name_help_text = _("inserta el dominio que quieres asignar al servidor.")
    log_help_text = _("Activa esta casilla si quieres recibir a tu cuenta de correo, "
                      "informes diarios sobre el estado del sitema, fallos en los servicios, "
                      "errores o actualizaciones. Podrás cambiar esta configuración "
                      "en cualquier momento desde la página de Notificaciones.")

    name        = forms.CharField(label=_('Nuevo dominio del servidor'),
                                 max_length=100, help_text=name_help_text, required=True)
    log_server = forms.BooleanField(label=_('Recibir Logs del sistema por correo electrónico'),
                                     help_text=log_help_text, required=False,
                                     widget=widgets.LabelledCheckbox(label=_('Recibir Logs')))

    def clean(self):
    ##############esto viene de get_form
        cleaned_data = super().clean()
        servername = self.servername
        domain  = self.cleaned_data.get('name')
        name = '%s.%s' % (servername, domain)
        if not re.match("^[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,20}$", name):
            context['display_form'] = True
            raise forms.ValidationError(
                _("%s no es un nombre de dominio válido" % name )
            )
        if name in self.domains:
            raise forms.ValidationError(
                _("El dominio %s ya está activado" % name)
            )
            context['display_form'] = True
        if name in utils.get_server_host():
            raise forms.ValidationError(
                _("%s ya es el dominio actual del sistema" % name)
          )
            context['display_form'] = True
        if name in self.domains_in_use:
            raise forms.ValidationError(
                _("El dominio %s está en uso por otra aplicación" % name)
          )
            context['display_form'] = True
        dns_records = utils.get_dns_records(name)
        record_a = utils.check_dns_A_record(dns_records)
        if record_a['error']:
            raise forms.ValidationError(
                _("La configuración de los DNS para El dominio %s no es correcta." % name)
          )
            context['display_form'] = True


    def __init__(self, *args, **kwargs):
        """ Send data to clean method for validation """
        self.servername = kwargs.pop('servername')
        self.domains = kwargs.pop('domain_list')
        self.domains_in_use = kwargs.pop('domains_used_list')
        super(FqdnForm,self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs = { 'placeholder' : 'example.com (sin %s)' % self.servername }

class EditAppForm(GenericForm):
    """ Form to edit user input dependencies for applications """

    def __init__(self,data=None, *args, **kwargs):
        appid = kwargs.pop('appid')
        inputdeps = kwargs.pop('inputdeps')
        maintenance = kwargs.pop('maintenance')
        super(EditAppForm, self).__init__(data,*args, **kwargs)
        for val in inputdeps:
            self.fields['%s' % val['id']] = forms.CharField(label=_(val['label']),help_text=_(val['Description']), required=True)
            #self.fields['domain'].choices = ((user, user) for user in users)
            self.fields['%s' % val['id']].widget.attrs = { 'data-app' : appid }
            if maintenance:
                self.fields['%s' % val['id']].widget.attrs = { 'readonly' : 'readonly' }
