# python
import json
# django
from django.utils.text import slugify
# project
from django.conf import settings
from .utils import p, get_puppet_status, get_release_info
from . import debug

def site_info_processor(request):
    """Injects into global context information about the site"""

    html_document_title       = settings.DOCUMENT_TITLE

    return locals()

def active_section_processor(request):
    """Injects into global context the slug of the path as an section identifier"""

    active_section = slugify(request.path.lstrip('/'))

    return locals()

def ldap_status(request):
    """Injects into global context installed services"""

    enabled_services     = []
    system_reboot        = False
    system_update        = False
    rocketchat_url       = ''
    notifications_number = 0
    apps_urls = {}
    apps_name_domain     = []

    if hasattr(request.user, 'role') and request.user.role == 'admin':
        apps_name_domain = request.apps_name_domain
        for app in apps_name_domain:
            app_url = '%s_url' % app
            apps_urls[app_url] = getattr(request, app_url)
        enabled_services = request.enabled_services
        system_reboot    = request.system_reboot
        if system_reboot:
            notifications_number += 1
        if(settings.NO_API):
            vm_status = debug.STATUS
            updates   = debug.UPDATES
        else:
            vm_status = request.status
            updates   = get_release_info(request, 'updates')
        system_update = vm_status == 'pending' or updates
        if system_update:
            notifications_number+=1

    return {
        'enabled_services'     : enabled_services,
        'apps_urls'            : apps_urls,
        'system_reboot'        : system_reboot,
        'system_update'        : system_update,
        'notifications_number' : notifications_number,
    }
